﻿using System.Text;
using System;


namespace Core
{
    public static class MAAPI
    {
        public struct SerialInfo
        {
            public string PortName;
            public int BaudRate;
            public int DataBits;
            public int DataLength;
            public int Parity;
            public int StopBit;
        }

        public static SerialInfo port;
        public static bool shutdown { get; set; }
        public static bool connected { get; set; }

        public static void Initialize()
        {
            port.PortName = "COM7";
            port.BaudRate = 115200;
            port.DataBits = 8;
            port.DataLength = 35;
            port.Parity = 0;
            port.StopBit = 1;
        }

        /// <summary>
        /// Scan to see if the port is ready
        /// </summary>
        /// <returns></returns>
        public static bool Scan()
        {
            bool flag = false;


            return flag;
        }

        /// <summary>
        /// Test Serial Port Connection
        /// </summary>
        /// <returns></returns>
        public static bool Connect()
        {
            bool flag = false;



            return flag;
        }

        public static string ReadData()
        {
            string data = "";

            while (!shutdown && connected)
            {
            }

            return data;
        }
    }
}
﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Hardware.Usb;
using System.Threading;
using Java.IO;

namespace MAAPI
{
    [Activity(Label = "MAAPI", MainLauncher = true, Icon = "@drawable/icon")]
    [BroadcastReceiver(Enabled = true)]
    [IntentFilter(new[] { Intent.ActionView },
        Categories = new[] { Intent.CategorySampleCode, "android.hardware.usb.action.USB_ACCESSORY_ATTACHED" })]
    public class MainActivity : Activity
    {
        struct Beacon
        {
            public string UID;
            public string IP;
            public string Lat;
            public string Long;
        };
        private Beacon currentBeacon = new Beacon();

        private bool shutdown { get; set; }
        private bool connected { get; set; }

        //private System.IO.Ports.SerialPort portCommSerial = null;
        private static String USB_ACCESSORY_ATTACHED = "android.hardware.usb.action.USB_ACCESSORY_ATTACHED";
        UsbDevice device;

        private ParcelFileDescriptor mFileDescriptor;
        private FileInputStream mInputStream;
        private FileOutputStream mOutputStream;
        private int freqCounter = 0;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our UI controls from the loaded layout:
            EditText txtIncomingData = FindViewById<EditText>(Resource.Id.txtIncomingData);
            Button btnConnect = FindViewById<Button>(Resource.Id.btnConnect);
            Button btnTransmit = FindViewById<Button>(Resource.Id.btnTransmit);

            /// Setup USB
            ///http://developer.android.com/guide/topics/connectivity/usb/accessory.html
            ///
            UsbManager usbManager = (UsbManager)GetSystemService(Context.UsbService);
            txtIncomingData.Append(String.Format("Total USB connected device(s): {0}\r\n", usbManager.DeviceList.Count));

            //PendingIntent mPermissionIntent;
            //mPermissionIntent = PendingIntent.GetBroadcast(this, 0, new Intent(USB_ACCESSORY_ATTACHED), 0);

            ///Enumerating accessories
            //UsbAccessory accessory = UsbManager.ActionUsbAccessoryAttached(Intent);
            UsbAccessory[] accessoryList = usbManager.GetAccessoryList();                                   //Get List
            UsbAccessory accessory = (UsbAccessory)Intent.GetParcelableExtra(UsbManager.ExtraAccessory);    //Get Single            

            //System.Collections.Generic.IDictionary<String, UsbDevice> deviceList = usbManager.DeviceList;
            //foreach (System.Collections.Generic.KeyValuePair<string, UsbDevice> entry in deviceList)
            //{
            //    txtIncomingData.Append(String.Format("Device = {0}\r\n", entry.Key));
            //    if (entry.Key == "Zedboard")
            //    {
            //        device = deviceList["<deviceName>"];
            //        usbManager.RequestPermission(device, mPermissionIntent);
            //    }
            //}


            if (accessory != null)
            {
                mFileDescriptor = usbManager.OpenAccessory(accessory);
                if (mFileDescriptor != null)
                {
                    FileDescriptor fd = mFileDescriptor.FileDescriptor;
                    mInputStream = new FileInputStream(fd);
                    mOutputStream = new FileOutputStream(fd);
                    Thread thread = new Thread(ReadData);
                    thread.Start();
                }
            }

            // Disable the "Transmit" button
            btnTransmit.Enabled = false;

            //Initialize COM port
            Core.MAAPI.Initialize();

            btnConnect.Click += (object sender, EventArgs e) =>
            {
                if (btnConnect.Text == "Connect")
                {
                    //Check to see if can communicate with the serial port
                    if (Core.MAAPI.Scan())
                    {
                        connected = Core.MAAPI.Connect();
                        //Clear text if disconnect
                        if (!connected)
                        {
                            txtIncomingData.Text = "";
                            txtIncomingData.Text = String.Format("Cannot connect port: {0}\r\n", Core.MAAPI.port.PortName);
                            connected = false;
                        }
                        else
                        {
                            //Successfully connected
                            txtIncomingData.Text = String.Format("Connected: {0}\r\n", Core.MAAPI.port.PortName);
                            btnConnect.Text = "Disconnect";
                            connected = true;
                        }
                    }
                    else
                    {
                        txtIncomingData.Text = String.Format("Cannot connect port: {0}\r\n", Core.MAAPI.port.PortName);
                        connected = false;
                    }
                }
                else
                {
                    btnConnect.Text = "Connect";
                    connected = false;
                }

                ///TODO: Create Read Thread
                if (connected)
                    ReadCOMPort();
            };
        }

        private void ReadCOMPort()
        {
            shutdown = false;
            Thread threadIP = new Thread(new ThreadStart(ReadData));
            threadIP.Start();
        }

        public void ReadData()
        {
            EditText txtIncomingData = FindViewById<EditText>(Resource.Id.txtCOMPortName);
            var receivedMsg = new byte[1];

            try
            {
                //Open Serial Port
                //portCommSerial = new System.IO.Ports.SerialPort(Core.MAAPI.port.PortName, Core.MAAPI.port.BaudRate, System.IO.Ports.Parity.None, Core.MAAPI.port.DataBits, System.IO.Ports.StopBits.One);
                //portCommSerial.DtrEnable = true;
                //portCommSerial.RtsEnable = true;
                //portCommSerial.ReadTimeout = 3000;

                while (!shutdown && connected)
                {
                    //string data = Core.MAAPI.ReadData();
                    mInputStream.Read();

                    string data = "";
                    string[] tokens;
                    string line = "";

                    //portCommSerial.Read(receivedMsg, 0, commConfig.DataLength);
                    while (data != "\n")
                    {
                        mInputStream.Read(receivedMsg, 0, 1);
                        data = System.Text.Encoding.Default.GetString(receivedMsg);
                        if (data != "\n")
                            line += data;
                    }

                    tokens = line.Split(',');
                    tokens = line.Split(',');
                    tokens = line.Split(',');
                    tokens = line.Split(',');
                    tokens = line.Split(',');
                    if (tokens.Length == 3 && tokens[0].Length == 11 && tokens[1].Length == 10 && tokens[2].Length == 11)
                    {
                        currentBeacon.UID = tokens[0].Trim().Replace("\0", "");
                        currentBeacon.Lat = tokens[1].Trim();
                        currentBeacon.Long = tokens[2].Trim();
                    }

                    Thread.Sleep(1000 * freqCounter);

                    txtIncomingData.Append(String.Format("> {0}\r\n", data));
                }
            }
            catch (Exception ex)
            {
                txtIncomingData.Append(String.Format("Error: {0}\r\n", ex.Message));
            }
        }
    }
}


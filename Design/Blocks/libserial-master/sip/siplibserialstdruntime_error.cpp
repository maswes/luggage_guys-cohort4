/*
 * Interface wrapper code.
 *
 * Generated by SIP 4.15.5 on Thu Apr 21 23:18:16 2016
 */

#include "sipAPIlibserial.h"

#line 23 "stdexcept.sip"
#include <stdexcept>
#line 12 "./siplibserialstdruntime_error.cpp"

#line 4 "string.sip"
#include <string>
#include <iostream>
#line 17 "./siplibserialstdruntime_error.cpp"


class sipstd_runtime_error : public std::runtime_error
{
public:
    sipstd_runtime_error(const std::string&);
    sipstd_runtime_error(const std::runtime_error&);
    virtual ~sipstd_runtime_error() throw();

    /*
     * There is a protected method for every virtual method visible from
     * this class.
     */
protected:
    const char * what() const throw();

public:
    sipSimpleWrapper *sipPySelf;

private:
    sipstd_runtime_error(const sipstd_runtime_error &);
    sipstd_runtime_error &operator = (const sipstd_runtime_error &);

    char sipPyMethods[1];
};

sipstd_runtime_error::sipstd_runtime_error(const std::string& a0): std::runtime_error(a0), sipPySelf(0)
{
    memset(sipPyMethods, 0, sizeof (sipPyMethods));
}

sipstd_runtime_error::sipstd_runtime_error(const std::runtime_error& a0): std::runtime_error(a0), sipPySelf(0)
{
    memset(sipPyMethods, 0, sizeof (sipPyMethods));
}

sipstd_runtime_error::~sipstd_runtime_error() throw()
{
    sipCommonDtor(sipPySelf);
}

const char * sipstd_runtime_error::what() const throw()
{
    sip_gilstate_t sipGILState;
    PyObject *sipMeth;

    sipMeth = sipIsPyMethod(&sipGILState,const_cast<char *>(&sipPyMethods[0]),sipPySelf,NULL,sipName_what);

    if (!sipMeth)
        return std::runtime_error::what();

    extern const char * sipVH_libserial_0(sip_gilstate_t, sipVirtErrorHandlerFunc, sipSimpleWrapper *, PyObject *, int);

    return sipVH_libserial_0(sipGILState, 0, sipPySelf, sipMeth, -8);
}


extern "C" {static PyObject *meth_std_runtime_error_what(PyObject *, PyObject *);}
static PyObject *meth_std_runtime_error_what(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;
    bool sipSelfWasArg = (!sipSelf || sipIsDerived((sipSimpleWrapper *)sipSelf));

    {
        const std::runtime_error *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "B", &sipSelf, sipType_std_runtime_error, &sipCpp))
        {
            const char *sipRes;

            sipRes = (sipSelfWasArg ? sipCpp->std::runtime_error::what() : sipCpp->what());

            if (sipRes == NULL)
            {
                Py_INCREF(Py_None);
                return Py_None;
            }

            return SIPBytes_FromString(sipRes);
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_runtime_error, sipName_what, NULL);

    return NULL;
}


/* Cast a pointer to a type somewhere in its superclass hierarchy. */
extern "C" {static void *cast_std_runtime_error(void *, const sipTypeDef *);}
static void *cast_std_runtime_error(void *ptr, const sipTypeDef *targetType)
{
    void *res;

    if (targetType == sipType_std_runtime_error)
        return ptr;

    if ((res = ((const sipClassTypeDef *)sipType_std_exception)->ctd_cast((std::exception *)(std::runtime_error *)ptr,targetType)) != NULL)
        return res;

    return NULL;
}


/* Call the instance's destructor. */
extern "C" {static void release_std_runtime_error(void *, int);}
static void release_std_runtime_error(void *sipCppV,int sipState)
{
    if (sipState & SIP_DERIVED_CLASS)
        delete reinterpret_cast<sipstd_runtime_error *>(sipCppV);
    else
        delete reinterpret_cast<std::runtime_error *>(sipCppV);
}


extern "C" {static void dealloc_std_runtime_error(sipSimpleWrapper *);}
static void dealloc_std_runtime_error(sipSimpleWrapper *sipSelf)
{
    if (sipIsDerived(sipSelf))
        reinterpret_cast<sipstd_runtime_error *>(sipGetAddress(sipSelf))->sipPySelf = NULL;

    if (sipIsPyOwned(sipSelf))
    {
        release_std_runtime_error(sipGetAddress(sipSelf),sipSelf->flags);
    }
}


extern "C" {static void *init_type_std_runtime_error(sipSimpleWrapper *, PyObject *, PyObject *, PyObject **, PyObject **, PyObject **);}
static void *init_type_std_runtime_error(sipSimpleWrapper *sipSelf, PyObject *sipArgs, PyObject *sipKwds, PyObject **sipUnused, PyObject **, PyObject **sipParseErr)
{
    sipstd_runtime_error *sipCpp = 0;

    {
        const std::string * a0;
        int a0State = 0;

        if (sipParseKwdArgs(sipParseErr, sipArgs, sipKwds, NULL, sipUnused, "J1", sipType_std_string,&a0, &a0State))
        {
            try
            {
            sipCpp = new sipstd_runtime_error(*a0);
            }
            catch (...)
            {
            sipReleaseType(const_cast<std::string *>(a0),sipType_std_string,a0State);
                sipRaiseUnknownException();
                return NULL;
            }
            sipReleaseType(const_cast<std::string *>(a0),sipType_std_string,a0State);

            sipCpp->sipPySelf = sipSelf;

            return sipCpp;
        }
    }

    {
        const std::runtime_error * a0;

        if (sipParseKwdArgs(sipParseErr, sipArgs, sipKwds, NULL, sipUnused, "J9", sipType_std_runtime_error, &a0))
        {
            try
            {
            sipCpp = new sipstd_runtime_error(*a0);
            }
            catch (...)
            {
                sipRaiseUnknownException();
                return NULL;
            }

            sipCpp->sipPySelf = sipSelf;

            return sipCpp;
        }
    }

    return NULL;
}


/* Define this type's super-types. */
static sipEncodedTypeDef supers_std_runtime_error[] = {{12, 255, 1}};


static PyMethodDef methods_std_runtime_error[] = {
    {SIP_MLNAME_CAST(sipName_what), meth_std_runtime_error_what, METH_VARARGS, NULL}
};


sipClassTypeDef sipTypeDef_libserial_std_runtime_error = {
    {
        -1,
        0,
        0,
        SIP_TYPE_CLASS,
        sipNameNr_std__runtime_error,
        {0}
    },
    {
        sipNameNr_runtime_error,
        {11, 255, 0},
        1, methods_std_runtime_error,
        0, 0,
        0, 0,
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    },
    0,
    -1,
    -1,
    supers_std_runtime_error,
    0,
    init_type_std_runtime_error,
    0,
    0,
#if PY_MAJOR_VERSION >= 3
    0,
    0,
#else
    0,
    0,
    0,
    0,
#endif
    dealloc_std_runtime_error,
    0,
    0,
    0,
    release_std_runtime_error,
    cast_std_runtime_error,
    0,
    0,
    0,
    0,
    0,
    0
};

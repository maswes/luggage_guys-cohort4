#include <SerialStream.h>
#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>
#include <cstring>

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

int
main( int    argc,
      char** argv  )
{
	std::ifstream in_stream; // fstream command to initiate "in_stream" as a command.
      	char strFilePath[30] = "uid";
	int sampleMessage = 10;
	int latPos = 3;
	int lngPos = 5;
	std::string uid;
	in_stream.open(strFilePath); // this in_stream (fstream) the "filename" to open.
	if (in_stream.fail())
	{
		std::cerr << "Error: Could not load uid file." << std::endl ;
		exit(1);
	}
	else
	{
		std::getline(in_stream, uid) ; 
		std::cerr << "UID: " << uid << " successfully loaded." << std::endl ;
	}

    //
    // Open the serial port.
    //
	std::cerr << "Testing: read serial port." 
		  << std::endl ;

    using namespace LibSerial ;
    SerialStream serial_port ;
    serial_port.Open( "/dev/ttyUSB0" ) ;
    if ( ! serial_port.good() ) 
    {
        std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
                  << "Error: Could not open serial port." 
                  << std::endl ;
        exit(1) ;
    }
    //
    // Set the baud rate of the serial port.
    //
    //serial_port.SetBaudRate( SerialStreamBuf::BAUD_115200 ) ;
    serial_port.SetBaudRate( SerialStreamBuf::BAUD_4800 ) ;
    if ( ! serial_port.good() ) 
    {
        std::cerr << "Error: Could not set the baud rate." << std::endl ;
        exit(1) ;
    }
    //
    // Set the number of data bits.
    //
    serial_port.SetCharSize( SerialStreamBuf::CHAR_SIZE_8 ) ;
    if ( ! serial_port.good() ) 
    {
        std::cerr << "Error: Could not set the character size." << std::endl ;
        exit(1) ;
    }
    //
    // Disable parity.
    //
    serial_port.SetParity( SerialStreamBuf::PARITY_NONE ) ;
    if ( ! serial_port.good() ) 
    {
        std::cerr << "Error: Could not disable the parity." << std::endl ;
        exit(1) ;
    }
    //
    // Set the number of stop bits.
    //
    serial_port.SetNumOfStopBits( 1 ) ;
    if ( ! serial_port.good() ) 
    {
        std::cerr << "Error: Could not set the number of stop bits."
                  << std::endl ;
        exit(1) ;
    }
    //
    // Turn off hardware flow control.
    //
    serial_port.SetFlowControl( SerialStreamBuf::FLOW_CONTROL_NONE ) ;
    if ( ! serial_port.good() ) 
    {
        std::cerr << "Error: Could not use hardware flow control."
                  << std::endl ;
        exit(1) ;
    }
    //
    // Do not skip whitespace characters while reading from the
    // serial port.
    //
    // serial_port.unsetf( std::ios_base::skipws ) ;
    //
    // Wait for some data to be available at the serial port.
    //
    while( serial_port.rdbuf()->in_avail() == 0 ) 
    {
        usleep(100) ;
    }
    //
    // Keep reading data from serial port and print it to the screen.
    // Read and parse for message:
    // 	$GPRMC,073054.000,A,3257.4927,N,11708.6728,W,0.00,26.24,210416,,,A*44
    //
    //while( serial_port.rdbuf()->in_avail() > 0  ) 
    int msgSize = 100 ;
    //char msg[msgSize];
    std::string msg = "";	
    std::string lat = "";	
    std::string lng = "";	
    char next_byte;
    bool beginRecording = false;
    bool isRMC = false;	

    for (int i = 0; i < sampleMessage; i++)
    {
	msg = "";	
	isRMC = false;
	next_byte = 0;

	//Read stream until end of line
    	while( next_byte != '\n'  ) 
	{
	     //Get next byte from stream	
	     serial_port.get(next_byte);

	     //Whether or not beginning of a GPS message
	     if (next_byte == '$')
	     {
		msg += next_byte;
		beginRecording = true;
	     }
	     //Start recording the message
	     else if (beginRecording == true && next_byte != '\n')
	     {
		msg += next_byte;
	     }
	     
	     //Whether or not the message is RMC
	     if (msg == "$GPRMC")
	     {
		isRMC = true;
	     }

	     //usleep(100) ;
        }
	if (isRMC == true)
	     break;
    }

    if (isRMC == true)
    {
	std::cerr << " msg:: " << msg << std::endl;
	std::vector<std::string> tokens = split(msg, ',');
	for(int i=0; i < tokens.size(); i++){
	   if (i == latPos)
		lat = tokens[i] + tokens[i+1];	//3252.9290N
	   if (i == lngPos)
		lng = tokens[i] + tokens[i+1];	//11714.0358W
	}
	
	std::string buf;
	//lat = "3252.9290N";
	//lng = "11714.0358W";
	buf = uid + ',' + lat + ',' + lng + '\n';

	char * output_items = (char *)malloc(35);
	//std::string buf;
	//buf = uid + ',' + lat + ',' + lng + '\n';

	//output_items = (unsigned char *)buf.c_str();
	//strcpy( static_cast <unsigned char*>( output_items ), buf );
	strncpy(output_items, buf.c_str(), 35);

	//(const unsigned char *)str.c_str();

    	std::cerr << "... UID = " << uid << "    LAT = " << lat << "    LONG = " << lng << std::endl ;
	std::cerr << "... Output = " << output_items;
    }
 
    std::cerr << "...End " << std::endl ;




    return EXIT_SUCCESS ;
}

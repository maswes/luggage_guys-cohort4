/* -*- c++ -*- */
/* 
 * Copyright 1970 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "gps_receive_impl.h"

#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>
#include <SerialStream.h>

using namespace std;
using namespace LibSerial;
namespace gr {
  namespace gps_blk {

    gps_receive::sptr
    gps_receive::make(float lat, float lng)
    {
      return gnuradio::get_initial_sptr
        (new gps_receive_impl(lat, lng));
    }

    /*
     * The private constructor
     */
    gps_receive_impl::gps_receive_impl(float lat, float lng)
      : gr::sync_block("gps_receive",
              gr::io_signature::make(0, 0, 0),
              gr::io_signature::make(1, 1, sizeof(char[20])))
    {}

    /*
     * Our virtual destructor.
     */
    gps_receive_impl::~gps_receive_impl()
    {
    }


    std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    	std::stringstream ss(s);
    	std::string item;
    	while (std::getline(ss, item, delim)) {
            elems.push_back(item);
    	}
    	return elems;
    }

    std::vector<std::string> split(const std::string &s, char delim) {
    	std::vector<std::string> elems;
    	split(s, delim, elems);
    	return elems;	
    }


    int
    gps_receive_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
	/// Output for GNU block
      	char *out = (char *) output_items[0];

	// Read UID data file
	std::ifstream in_stream; // fstream command to initiate "in_stream" as a command.
      	char strFilePath[30] = "uid";		//"/home/vahid/Desktop/OFDM/uid"
	std::string uid;
	int msgSize = 100 ;
	char next_byte;
	bool beginRecording = false;
	bool isRMC = false;	
	int sampleMessage = 10;
	int latPos = 3;
	int lngPos = 5;

	in_stream.open(strFilePath); // this in_stream (fstream) the "filename" to open.
	if (in_stream.fail())
	{
		std::cerr << "Error: Could not load uid file." << std::endl ;
		exit(1);
	}
	else
	{
		std::getline(in_stream, uid) ; 
		std::cerr << "UID: " << uid << " successfully loaded." << std::endl ;
	}
	
      	// Do <+signal processing+>
	SerialStream serial_port ;
	serial_port.Open( "/dev/ttyUSB0" ) ;
	if ( ! serial_port.good() ) 
	{
		std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
			  << "Error: Could not open serial port." 
			  << std::endl ;
		exit(1) ;
	}
	//
	// Set the baud rate of the serial port.
	//
	serial_port.SetBaudRate( SerialStreamBuf::BAUD_115200 ) ;
	if ( ! serial_port.good() ) 
	{
		std::cerr << "Error: Could not set the baud rate." << std::endl ;
		exit(1) ;
	}
	//
	// Set the number of data bits.
	//
	serial_port.SetCharSize( SerialStreamBuf::CHAR_SIZE_8 ) ;
	if ( ! serial_port.good() ) 
	{
		std::cerr << "Error: Could not set the character size." << std::endl ;
		exit(1) ;
	}
	//
	// Disable parity.
	//
	serial_port.SetParity( SerialStreamBuf::PARITY_NONE ) ;
	if ( ! serial_port.good() ) 
	{
		std::cerr << "Error: Could not disable the parity." << std::endl ;
		exit(1) ;
	}
	//
	// Set the number of stop bits.
	//
	serial_port.SetNumOfStopBits( 1 ) ;
	if ( ! serial_port.good() ) 
	{
		std::cerr << "Error: Could not set the number of stop bits."
			  << std::endl ;
		exit(1) ;
	}
	//
	// Turn off hardware flow control.
	//
	serial_port.SetFlowControl( SerialStreamBuf::FLOW_CONTROL_NONE ) ;
	if ( ! serial_port.good() ) 
	{
		std::cerr << "Error: Could not use hardware flow control."
			  << std::endl ;
		exit(1) ;
	}
	//
	// Do not skip whitespace characters while reading from the
	// serial port.
	//
	// serial_port.unsetf( std::ios_base::skipws ) ;
	//
	// Wait for some data to be available at the serial port.
	//
	while( serial_port.rdbuf()->in_avail() == 0 ) 
	{
		usleep(100) ;
	}
	//
	// Keep reading data from serial port and print it to the screen.
	//
	// Read and parse for message:
	// 	$GPRMC,073054.000,A,3257.4927,N,11708.6728,W,0.00,26.24,210416,,,A*44
	//
	//char msg[msgSize];
	std::string msg = "";	
	std::string lat = "";	
	std::string lng = "";	

	for (int i = 0; i < sampleMessage; i++)
	{
		msg = "";	
		isRMC = false;
		next_byte = 0;

		//Read stream until end of line
		while( next_byte != '\n'  ) 
		{
		     //Get next byte from stream	
		     serial_port.get(next_byte);

		     //Whether or not beginning of a GPS message
		     if (next_byte == '$')
		     {
			msg += next_byte;
			beginRecording = true;
		     }
		     //Start recording the message
		     else if (beginRecording == true && next_byte != '\n')
		     {
			msg += next_byte;
		     }
		     
		     //Whether or not the message is RMC
		     if (msg == "$GPRMC")
		     {
			isRMC = true;
		     }

		     //usleep(100) ;
		}
		if (isRMC == true)
		     break;
	}

	if (isRMC == true)
	{
		//std::cerr << " msg:: " << msg << std::endl;
		std::vector<std::string> tokens = split(msg, ',');
		for(int i=0; i < tokens.size(); i++){
		   if (i == latPos)
			lat = tokens[i] + tokens[i+1];	//3252.9290N
		   if (i == lngPos)
			lng = tokens[i] + tokens[i+1];	//11714.0358W
		}
		std::string buf;
		buf = uid + ',' + lat + ',' + lng + '\n';
		out = (char *)malloc(35);
		strncpy(out, buf.c_str(), 35);

		//std::cerr << "... UID = " << uid << "    LAT = " << lat << "    LONG = " << lng << std::endl ;
	}

	//std::cerr << "...End " << std::endl ;



      	// Tell runtime system how many output items we produced.
      	return noutput_items;
    }

  } /* namespace gps_blk */
} /* namespace gr */


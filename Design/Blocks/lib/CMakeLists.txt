# Copyright 2011,2012 Free Software Foundation, Inc.
#Building CXX
# This file is part of GNU Radio
#
# GNU Radio is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# GNU Radio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNU Radio; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.

########################################################################
# Setup library
########################################################################
include(GrPlatform) #define LIB_SUFFIX

include_directories(${Boost_INCLUDE_DIR})
include_directories(/home/vahid/Desktop/Blocks/libserial-master/src)
#link_directories(${Boost_LIBRARY_DIRS})
link_directories(${Boost_LIBRARY_DIRS})
link_directories(/home/vahid/Desktop/Blocks/libserial-master/src)
list(APPEND gps_blk_sources
    gps_receive_impl.cc
)

set(gps_blk_sources "${gps_blk_sources}" PARENT_SCOPE)
if(NOT gps_blk_sources)
	MESSAGE(STATUS "No C++ sources... skipping lib/")
	return()
endif(NOT gps_blk_sources)

add_library(gnuradio-gps_blk SHARED ${gps_blk_sources})
target_link_libraries(
	gnuradio-gps_blk 
	${Boost_LIBRARIES} 
	${GNURADIO_ALL_LIBRARIES}
)
set_target_properties(gnuradio-gps_blk PROPERTIES DEFINE_SYMBOL "gnuradio_gps_blk_EXPORTS" )

if(APPLE)
    set_target_properties(gnuradio-gps_blk PROPERTIES
        INSTALL_NAME_DIR "${CMAKE_INSTALL_PREFIX}/lib"
    )
endif(APPLE)

########################################################################
# Install built library files
########################################################################
install(TARGETS gnuradio-gps_blk
    LIBRARY DESTINATION lib${LIB_SUFFIX} # .so/.dylib file
    ARCHIVE DESTINATION lib${LIB_SUFFIX} # .lib file
    RUNTIME DESTINATION bin              # .dll file
)

########################################################################
# Build and register unit test
########################################################################
include(GrTest)

include_directories(${CPPUNIT_INCLUDE_DIRS})

list(APPEND test_gps_blk_sources
    ${CMAKE_CURRENT_SOURCE_DIR}/test_gps_blk.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/qa_gps_blk.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/qa_gps_receive.cc
)

add_executable(test-gps_blk ${test_gps_blk_sources})

target_link_libraries(
  test-gps_blk
  ${GNURADIO_RUNTIME_LIBRARIES}
  ${Boost_LIBRARIES}
  ${CPPUNIT_LIBRARIES}
  gnuradio-gps_blk
)

GR_ADD_TEST(test_gps_blk test-gps_blk)

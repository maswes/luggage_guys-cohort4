﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="LuggageWebApp.Contact" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        <h1><%: Title %>.</h1>
        <h2>The Luggage Guys Team.</h2>
    </hgroup>


    <section id="Member1">
        <section class="contact">
            <header>
                <h3>Andrew Lanez</h3>
            </header>
            <p>
                <asp:Image ID="Image1" ImageUrl="/Images/Andrew.png" runat="server" />
            </p>
        </section>

        <section class="contact">
            <header>
                <h3>Grad Student</h3>
            </header>
            <p>
                <span class="label">Electrical Engineer</span><br />
                <span>Wireless Communications Specialist</span><br />
                <span>Class of 2016</span>
            </p>
        </section>
    </section>

    <section id="Member2">
        <section class="contact">
            <header>
                <h3>Man Ho</h3>
            </header>
            <p>
                <asp:Image ID="Image2" ImageUrl="/Images/Man.png" runat="server" />
            </p>
        </section>

        <section class="contact">
            <header>
                <h3>Grad Student</h3>
            </header>
            <p>
                <span class="label">Software Engineer</span><br />
                <span>Embedded Systems Specialist</span><br />
                <span>Class of 2016</span>
            </p>
        </section>
    </section>

    <section id="Member3">
        <section class="contact">
            <header>
                <h3>Vahid Nazer</h3>
            </header>
            <p>
                <asp:Image ID="Image3" ImageUrl="/Images/Vahid.png" runat="server" />
            </p>
        </section>

        <section class="contact">
            <header>
                <h3>Grad Student</h3>
            </header>
            <p>
                <span class="label">Software Engineer</span><br />
                <span>Applications/Embedded Software Specialist</span><br />
                <span>Class of 2016</span>
            </p>
        </section>
    </section>
</asp:Content>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LuggageWebApp.Account
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Login_Click(object sender, EventArgs e)
        {
            string username;
            string password;
            try
            {
                username = LoginForm.UserName;
                password = LoginForm.Password;
                if (username.ToUpper() == "WES" && password.ToUpper() == "COHORT4")
                {
                    Session["USERNAME"] = username;
                    Response.Redirect("~/Geo/");
                }
                else
                {
                    Response.Redirect("~/Account/Login.aspx");
                }
            }
            catch
            { }
        }
    }
}
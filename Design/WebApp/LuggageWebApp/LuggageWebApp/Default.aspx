﻿<%@ Page Title="Luggage Guys" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="LuggageWebApp._Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">                
                <h2>A revolutionary wireless luggage tracking system.</h2>
            </hgroup>
            <p>
                This project features a revolutionary in-baggage global tracking system through the use of wireless embedded systems, 
                Internet of Things (IoT), and the cloud. This technology helps passengers travel with confidence. Should their luggage 
                ever get misdirected, it can easily be located for quick retrieval.
            </p>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <section id="Overview">
        <h3>Objectives:</h3>
        <ol class="round">
            <li class="one">
                <h5>Wireless Communication</h5>
                The ability to reliably transmit data from the beacon device to the receiver.
            </li>
            <li class="two">
                <h5>Global Positioning System (GPS) Tracking</h5>
                Use of a GPS antenna to acquire luggage position which is reported wirelessly to the cloud.
            </li>
            <li class="three">
                <h5>Real-time Location Mapping</h5>
                Plot real-time luggage location on Google Maps.
            </li>
        </ol>    
    </section>
    <section id="BurningPhoto">
        <h3>The Burning Platform</h3><br />
        <asp:Image ID="burningform" ImageUrl="~/Images/burningplatform.png" runat="server" />
    </section>

</asp:Content>

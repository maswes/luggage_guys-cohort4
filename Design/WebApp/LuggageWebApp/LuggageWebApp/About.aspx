﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="LuggageWebApp.About" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        <h1><%: Title %>.</h1>
        <h2>Our project aims to provide an integrated technology solution that helps travelers connect to and track their luggage in real-time.
        </h2>
    </hgroup>

    <article>
        <p>        
            View our prototype demo below.
        </p>

        <p>        
            <iframe  src="https://www.youtube.com/embed/gwjC6f0Tvys" frameborder="0" allowfullscreen></iframe>
        </p>

    </article>

    <aside>
        <h3>Our Mission</h3>
        <p>        
            Give companies and end-users the power to track and locate their valuable assets worldwide in real-time. 
            Using innovative and evolving technologies, The Luggage Guys services provide a strategic platform on 
            which companies gain visibility and control of their assets.
        </p>
    </aside>
</asp:Content>
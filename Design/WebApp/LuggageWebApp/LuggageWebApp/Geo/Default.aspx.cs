﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using MyWCFServices;
using GeoLocation;
using System.Timers;

namespace LuggageWebApp.Geo
{
    //Tutorial: https://developers.google.com/maps/documentation/javascript/tutorials/earthquakes#circle_size
    //Tutorial (KML): https://developers.google.com/maps/documentation/javascript/tutorials/kml#the_entire_code

    public partial class Default : System.Web.UI.Page
    {
        /// <summary>
        /// The thread object used to poll Geo Location data.
        /// </summary>
        /// 
        protected GeoData postedGeoData;
        protected Thread threadPollGeo = null;
        protected int SleepTime = 1000;
        protected bool Stop = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["USERNAME"] == null)
            {
                Response.Redirect("~\\Account\\Login.aspx");
            }
            else if (Session["USERNAME"].ToString().ToUpper() != "WES")
            {
                Response.Redirect("~\\Account\\Login.aspx");
            }

            if (!IsPostBack)
            {
            }
        }

        //private void ProcessGeoData()
        //{
        //    try
        //    {
        //        GeoDatatAPIService wsGeo = new GeoDatatAPIService();
        //        int i = 0;
        //        //while(!Stop)
        //        //{
        //            postedGeoData = wsGeo.GetGeoData();
        //            if (postedGeoData != null)
        //            {
        //                //String.Format("The current price is {0} per ounce.",pricePerOunce);
        //                txtGeoData.Text = String.Format(">  {0}\t{1}\t{2}\t{3} \t{4}\t{5} \t{6}\n", postedGeoData.RecordedDate.ToShortDateString()
        //                    , postedGeoData.RecordedTime.ToLongTimeString(), postedGeoData.UID, postedGeoData.GetLatitude(), postedGeoData.LATDirection,
        //                    postedGeoData.GetLongitude(), postedGeoData.LONGDirection) + txtGeoData.Text;                        
        //            }
        //            if (i == 100)
        //            {
        //                i = 0;
        //                Stop = true;
        //            }
        //            //i++;
        //            //System.Threading.Thread.Sleep(SleepTime);
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        this.txtGeoData.Text += '\n' + ex.Message;
        //    }
        //}

        //protected void btnViewMap_Click(object sender, EventArgs e)
        //{
        //    // Start the thread that performs the polling.
        //    //threadPollGeo = new Thread(new ThreadStart(ProcessGeoData));
        //    //threadPollGeo.Start();

        //    GeoTimer.Enabled = true;
        //}

        //protected void Timer1_Tick(object sender, EventArgs e)
        //{
        //    ProcessGeoData();
        //}

        //protected void btnStop_Click(object sender, EventArgs e)
        //{
        //    GeoTimer.Enabled = false;
        //}
    }
}
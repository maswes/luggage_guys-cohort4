﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Map.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="LuggageWebApp.Geo.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="src/geolocation-marker.js"></script>

    <script type ="text/javascript">
        var map;
        var infowindow;
        function InitializeMap() {
            var latlng = new google.maps.LatLng(32.880635, -117.234063);
            var myOptions =
            {
                zoom: 15,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            if (document.getElementById("map") != null)
            {
                map = new google.maps.Map(document.getElementById("map"), myOptions);
            }
        }


        function markicons() {

            InitializeMap();

            if (map == null)
                return;

            var ltlng = [];

            ltlng.push(new google.maps.LatLng(32.880635, -117.234063));
            ltlng.push(new google.maps.LatLng(32.878065, -117.230567));
            ltlng.push(new google.maps.LatLng(32.875638, -117.235998));

            //map.setCenter(ltlng[0]);
            for (var i = 0; i <= ltlng.length; i++) {
                /*
                if (i > 0)
                {
                    marker = new google.maps.Marker({
                        map: map,
                        position: ltlng[i],
                        icon: "../Images/mark_blue.png"
                        });
                }
                else
                {
                    marker = new google.maps.Marker({
                        map: map,
                        position: ltlng[i]
                    });
                }
                */

                var GeoMarker = new GeolocationMarker();
                GeoMarker.setCircleOptions({ fillColor: '#808080' });

                google.maps.event.addListenerOnce(GeoMarker, 'position_changed', function () {
                    map.setCenter(this.getPosition());
                    map.fitBounds(this.getBounds());
                });

                GeoMarker.setMap(map);

                (function (i, marker) {

                    google.maps.event.addListener(marker, 'click', function () {

                        if (!infowindow) {
                            infowindow = new google.maps.InfoWindow();
                        }

                        infowindow.setContent("UID: " + "Cohort 4");

                        infowindow.open(map, marker);

                    });

                })(i, marker);

            }
        }

        //window.onload = markicons; 
        google.maps.event.addDomListener(window, 'load', InitializeMap);
        setInterval(markicons, 5000);
        markicons();

    </script>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <asp:Button ID="btnViewMap" runat="server" Text="View Map" OnClick="btnViewMap_Click" />
                <asp:Button ID="btnStop" runat="server" Text="Stop" OnClick="btnStop_Click" />
                <asp:TextBox ID="txtGeoData" runat="server" Rows="5" TextMode="MultiLine" Width="960px"></asp:TextBox>
                <asp:Timer ID="GeoTimer" runat="server" Interval="1000" OnTick="Timer1_Tick" Enabled="False">
                </asp:Timer>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    <div id ="map" style="width: 960px; height: 400px"></div>

</asp:Content>

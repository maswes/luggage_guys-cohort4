﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeoLocation
{
    public class GeoData
    {
        #region Private Variables

        private DateTime datRecordedDate = DateTime.Now.Date;
        private DateTime timRecordedTime = DateTime.Now.ToLocalTime();
        private String strUID = "";
        private double dblLAT = 0.0;
        private String strLAT = "";
        private String strLATDirection = "";
        private double dblLNG = 0.0;
        private String strLNG = "";
        private String strLNGDirection = "";

        #endregion


        #region Constructor

        public GeoData()
        { }

        #endregion


        #region Properties

        /// <summary>
        /// Date of receiving Geo-Location data
        /// </summary>
        public DateTime RecordedDate
        {
            get
            {
                return datRecordedDate;
            }
            set
            {
                datRecordedDate = value;
            }
        }

        /// <summary>
        /// Time of receiving Geo-Location data
        /// </summary>
        public DateTime RecordedTime
        {
            get
            {
                return timRecordedTime;
            }
            set
            {
                timRecordedTime = value;
            }
        }

        /// <summary>
        /// Unique identifier of Geo-Location data owner
        /// </summary>
        public String UID
        {
            get
            {
                return strUID;
            }
            set
            {
                strUID = value;
            }
        }

        /// <summary>
        /// Latitude of Geo-Location data
        /// </summary>
        public String Latitude
        {
            get
            {
                return strLAT;
            }
            set
            {
                strLAT = value;
                dblLAT = 0.0;
                try
                {
                    dblLAT = Convert.ToDouble(value);
                }
                catch { }
            }
        }

        /// <summary>
        /// Latitude direction of Geo-Location data
        /// </summary>
        public String LATDirection
        {
            get
            {
                return strLATDirection;
            }
            set
            {
                strLATDirection = value;
            }
        }

        /// <summary>
        /// Longitude direction of Geo-Location data
        /// </summary>
        public String Longtitude
        {
            get
            {
                return strLNG;
            }
            set
            {
                strLNG = value;
                dblLNG = 0.0;
                try
                {
                    dblLNG = Convert.ToDouble(value);
                }
                catch { }
            }
        }       
        
        /// <summary>
                 /// Longitude direction of Geo-Location data
                 /// </summary>
        public String LONGDirection
        {
            get
            {
                return strLNGDirection;
            }
            set
            {
                strLNGDirection = value;
            }
        }

        #endregion


        #region Private Methods

        /// <summary>
        /// Convert GPRMC data value to double 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        //Function GPRMC2Degrees(Value As Double) As Double GPRMC2Degrees = Int(Value / 100) + (Value - (Int(Value / 100) * 100)) / 60 End Function
        private double GPRMC2Degrees(double value)
        {
            double result = 0;

            result = (int)(value / 100) + (value - ((int)(value / 100) * 100)) / 60;

            return result;
        }

        #endregion


        #region Public Method

        /// <summary>
        /// Get Latitude number value
        /// </summary>
        /// <returns></returns>
        public double GetLatitude()
        {
            return GPRMC2Degrees(dblLAT);
        }

        /// <summary>
        /// Get Longitude number value
        /// </summary>
        /// <returns></returns>
        public double GetLongitude()
        {
            return GPRMC2Degrees(dblLNG);
        }

        #endregion
    }
}

﻿using System;
using System.IO;
using System.Xml;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Threading.Tasks;
using GeoLocation;
using System.Xml.Linq;
using System.Web;

namespace MyWCFServices
{
    public class GeoDatatAPIService: IGeoDatatAPIService
    {
        private GeoData myData = new GeoData();
        //private string strKMLSource = "C:\\temp\\luggage_guys.kml";
        private string strKMLSource = "luggage_guys.kml";
        //private string strPHPSource = "App_Data\\getdata.php";
        private string strPHPSource = "web_root\\getdata.php";

        public void PostGeoData(DateTime datRecordedDate, DateTime timRecordedTime, String strUID, String strLAT, String strLATDirection, String strLNG, String strLNGDirection)
        {
            myData.RecordedDate = datRecordedDate;
            myData.RecordedTime = timRecordedTime;
            myData.UID = strUID;
            myData.Latitude = strLAT;
            myData.LATDirection = strLATDirection;
            myData.Longtitude = strLNG;
            myData.LONGDirection = strLNGDirection;
            //WriteKMLFile();
            WritePHPDataFile();
        }
    
        public GeoData GetGeoData()
        {
            return myData;
        }

        private string WritePHPDataFile()
        {
            ///////////////////////////////////////////////////////
            //// Sample file
            ///////////////////////////////////////////////////////
            //<?php
            //$data = "LOCAL 32.880635 -117.234063";
            //
            //header("Content-type: text/javascript");
            //header('Content-Encoding: gzip');
            //echo gzencode($data);
            //?>
            ///////////////////////////////////////////////////////

            string ns = ""; //North-South
            string ew = ""; //East-West
            string coord = ""; //Coordinates
            string status = "";

            try
            {

                #region Calculate coordinate

                if (myData.LATDirection == "S")
                    ns = "-";
                else
                    ns = "";

                if (myData.LONGDirection == "W")
                    ew = "-";
                else
                    ew = "";
                //coord = ew + myData.GetLongitude() + "," + ns + myData.GetLatitude() + ", 0";
                coord = ns + myData.GetLatitude() + " " + ew + myData.GetLongitude();

                #endregion

                #region Write PHP

                // Create a file to write to. 
                string createText = "<?php" + Environment.NewLine;
                createText += "$data = \"LOCAL " + coord + "\";" + Environment.NewLine;
                createText += "header(\"Content-type: text/javascript\");" + Environment.NewLine;
                createText += "header('Content-Encoding: gzip');" + Environment.NewLine;
                createText += "echo gzencode($data);" + Environment.NewLine;
                createText += "?>" + Environment.NewLine;
                //string strPath = AppDomain.CurrentDomain.BaseDirectory + strPHPSource;
                //string strPath = System.Web.HttpContext.Current.Server.MapPath(strPHPSource);                                                    
                string strPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + strPHPSource;
                File.WriteAllText(strPath, createText);
                status = "Success: " + strPath;

                #endregion 
            }
            catch (Exception ex)
            {
                string str = ex.Message;
                status = str;
            }

            return status;
        }

        private void WriteKMLFile()
        {
            string ns = ""; //North-South
            string ew = ""; //East-West
            string coord = ""; //Coordinates
            string lastCoord = "";      //Last row coordinate
            string strPath = "";

            try
            {

                if (myData.LATDirection == "S")
                    ns = "-";
                else
                    ns = "";

                if (myData.LONGDirection == "W")
                    ew = "-";
                else
                    ew = "";
                coord = ew + myData.GetLongitude() + "," + ns + myData.GetLatitude() + ", 0";

                if (File.Exists(strKMLSource) == false)
                {
                    XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
                    xmlWriterSettings.Indent = true;
                    xmlWriterSettings.NewLineOnAttributes = true;

                    strPath = AppDomain.CurrentDomain.BaseDirectory + strKMLSource;

                    using (XmlWriter xmlWriter = XmlWriter.Create(strPath, xmlWriterSettings))
                    {
                        #region Create new KML file

                        // Opens the document
                        xmlWriter.WriteStartDocument(true);

                        // Write KML
                        //xmlWriter.WriteStartElement("kml", "http://www.opengis.net/kml/2.2");
                        xmlWriter.WriteStartElement("kml");
                        xmlWriter.WriteStartElement("Document");
                        xmlWriter.WriteStartElement("Name");
                        xmlWriter.WriteString("LuggageGuys KML");
                        xmlWriter.WriteEndElement(); // Name

                        //https://www.google.com/fusiontables/DataSource?dsrcid=308519#map:id=3
                        //http://www.google.com/mapfiles/marker.png
                        //https://sites.google.com/site/gmapicons/home/

                        xmlWriter.WriteStartElement("Style", "#luggage_style");
                        xmlWriter.WriteStartElement("IconStyle");
                        xmlWriter.WriteStartElement("Icon");
                        xmlWriter.WriteStartElement("href");                   
                        //xmlWriter.WriteString("https://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png");
                        xmlWriter.WriteString("http://www.theluggageguys/Images/SingleBag_Small.png");
                        xmlWriter.WriteEndElement(); // href
                        xmlWriter.WriteEndElement(); // Icon
                        xmlWriter.WriteEndElement(); // IconStyle
                        xmlWriter.WriteStartElement("BalloonStyle");
                        xmlWriter.WriteStartElement("text");
                        xmlWriter.WriteString("$[info]");
                        xmlWriter.WriteEndElement(); // text
                        xmlWriter.WriteEndElement(); // BalloonStyle
                        xmlWriter.WriteEndElement(); // Style

                        xmlWriter.WriteStartElement("Style", "#path_style");
                        xmlWriter.WriteStartElement("IconStyle");
                        xmlWriter.WriteStartElement("Icon");
                        xmlWriter.WriteStartElement("href");
                        //xmlWriter.WriteString("https://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png");
                        xmlWriter.WriteString("http://www.theluggageguys/Images/small_blue.png");
                        xmlWriter.WriteEndElement(); // href
                        xmlWriter.WriteEndElement(); // Icon
                        xmlWriter.WriteEndElement(); // IconStyle
                        xmlWriter.WriteStartElement("BalloonStyle");
                        xmlWriter.WriteStartElement("text");
                        xmlWriter.WriteEndElement(); // text
                        xmlWriter.WriteEndElement(); // BalloonStyle
                        xmlWriter.WriteEndElement(); // Style

                        //xmlWriter.WriteStartElement("Placemark");
                        //xmlWriter.WriteStartElement("Name");
                        //xmlWriter.WriteString(myData.UID);
                        //xmlWriter.WriteEndElement();    //Name
                        //xmlWriter.WriteStartElement("styleUrl");
                        //xmlWriter.WriteString("#luggage_style");
                        //xmlWriter.WriteEndElement();    //styleUrl

                        ////Write Extended data element
                        //xmlWriter.WriteStartElement("ExtendedData");
                        //xmlWriter.WriteStartElement("Data","info");
                        //xmlWriter.WriteStartElement("value");
                        //xmlWriter.WriteString(myData.UID);
                        //xmlWriter.WriteEndElement();    //value
                        //xmlWriter.WriteEndElement();    //Data
                        //xmlWriter.WriteEndElement();    //ExtendedData

                        //// Write Point element
                        //xmlWriter.WriteStartElement("Point");
                        //xmlWriter.WriteStartElement("coordinates");
                        //xmlWriter.WriteString(coord);
                        //xmlWriter.WriteEndElement();
                        //xmlWriter.WriteEndElement();

                        //xmlWriter.WriteEndElement(); // Placemark
                        xmlWriter.WriteEndElement(); // Document
                        xmlWriter.WriteEndElement(); // kml

                        // Ends the document.
                        xmlWriter.WriteEndDocument();
                        xmlWriter.Flush();

                        // close writer
                        xmlWriter.Close();

                        #endregion
                    }
                }
                else
                {
                    #region Open KML file

                    XDocument xDocument = XDocument.Load(strPath);
                    //IEnumerable<XElement> root1 = xDocument.Elements("kml");
                    //XElement root = xDocument.Element("kml");
                    IEnumerable<XElement> rows = xDocument.Descendants("kml").Descendants("Document").Descendants("Placemark");
                    XElement lastRow = rows.Last();
                    XElement eleStyle = null;

                    //Update existing place mark icon
                    foreach (XElement ele in lastRow.Descendants())
                    {
                        if (ele.Name == "styleUrl")
                        {
                            //ele.Value = "path_style";
                            eleStyle = ele;
                        }
                        else if (ele.Name == "coordinates")
                        {
                            lastCoord = ele.Value.Trim();
                        }
                    }

                    if (lastCoord != coord)
                    {
                        //Add new place mark
                        lastRow.AddAfterSelf(
                           new XElement("Placemark",
                               new XElement("name", myData.UID),
                               new XElement("styleUrl", "#luggage_style"),
                               new XElement("ExtendedData",
                                   new XElement("Data", new XAttribute("name", "info"),
                                       new XElement("value", myData.UID)
                                   )                               
                               ),
                               new XElement("Point",
                                   new XElement("coordinates", coord)
                               )
                            )
                        );
                        eleStyle.Value = "#path_style";
                    }

                    //Save KML file
                    xDocument.Save(strKMLSource);

                    #endregion
                }
            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
        }
    }
}

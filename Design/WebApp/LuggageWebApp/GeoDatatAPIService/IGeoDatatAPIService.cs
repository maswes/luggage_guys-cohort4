﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using GeoLocation;

namespace MyWCFServices
{
    [ServiceContract]
    public interface IGeoDatatAPIService
    {
        //DateTime datRecordedDate
        //DateTime timRecordedTime
        //String strUID
        //String strLAT
        //String strLATDirection
        //String strLNG
        //String strLNGDirection

        [OperationContract]
        void PostGeoData(DateTime datRecordedDate, DateTime timRecordedTime, String strUID, String strLAT, String strLATDirection, String strLNG, String strLNGDirection);

        [OperationContract]
        GeoData GetGeoData();
    }
}

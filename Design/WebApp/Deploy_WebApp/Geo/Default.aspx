﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Map.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="LuggageWebApp.Geo.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        #map-canvas {     
          height: 100%;     
          width: 100%;     
          margin: 0px;
          padding: 0px
        }
    </style>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=visualization"></script>

    <script type="text/javascript">
        var map, initCenter, initOptions, myMarkers = [], ltlng = [], lookup = [];
        var viewTrail = false;

        function initialize() {
            initCenter = new google.maps.LatLng(32.880635, -117.234063);
            initOptions = {
                zoom: 17,
                center: initCenter,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById('map-canvas'), initOptions);
        }

        // Parses the binary encoded data passed to this script by getdata.php
        function parseData(raw) {
            var LatLon, marker
            var nodes = raw.split("  ");
            //Erases all markers
            for (i = 0; i < myMarkers.length; ++i) {
                myMarkers[i].setMap(null);
            }
            for (i = 0; i < nodes.length; ++i) {
                nodes[i] = nodes[i].split(" ");
                var search = [nodes[i][1], nodes[i][2]];
                if (isExisted(search) === false) {
                    //Clear arrays
                    if (viewTrail === false) {
                        myMarkers = [];
                        ltlng = [];
                        lookup = [];
                    }

                    lookup.push([nodes[i][1], nodes[i][2]]);
                    LatLon = new google.maps.LatLng(nodes[i][1], nodes[i][2]);
                    ltlng.push(LatLon);
                }
                //Call to plot points
                markicons();
            }
        }

        function markicons() {
            if (map == null)
                return;
            for (var i = 0; i <= ltlng.length; i++) {
                if (i < ltlng.length - 1) {
                    marker = new google.maps.Marker({
                        map: map,
                        position: ltlng[i],
                        icon: "../Images/small_blue.png"
                    });
                }
                else {
                    marker = new google.maps.Marker({
                        map: map,
                        position: ltlng[i],
                        icon: "../Images/SingleBag_Small.png"
                    });
                }

                (function (i, marker) {
                    google.maps.event.addListener(marker, 'click', function () {
                        if (!infowindow) {
                            infowindow = new google.maps.InfoWindow();
                        }
                        infowindow.setContent("UID: " + "Cohort 4");
                        infowindow.open(map, marker);
                    });
                })(i, marker);

                myMarkers.push(marker);     //Save marker for erase later
            }
        }

        // Check for existing. Return true if existed; false, otherwise
        function isExisted(search) {
            for (var i = 0, l = lookup.length; i < l; i++) {
                if (lookup[i][0] === search[0] && lookup[i][1] === search[1]) {
                    return true;
                }
            }
            return false;
        }

        // Makes the xml http request compatible with various browsers
        function getXMLHttpRequest() {
            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            };
            return xmlhttp;
        }

        // Sends a server request for data - calls getdata.php on the server
        function getData() {
            var xmlhttp = getXMLHttpRequest(), raw;

            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    raw = xmlhttp.responseText;
                    parseData(raw);
                    //"LOCAL 32.999997 -109.999996"
                    //parseData("LOCAL 32.999997 -109.999996");
                    //parseData("LOCAL 32.880635 -117.234063");
                }
            }

            xmlhttp.open("GET", "../web_root/getdata.php?t=" + Math.random(), true);
            xmlhttp.send();
        }

        google.maps.event.addDomListener(window, 'load', initialize);
        setInterval(getData, 500);
        getData();
    </script>
    <div>
        <asp:HyperLink ID="lnkViewLargerMap" NavigateUrl="../web_root/" runat="server">View larger map</asp:HyperLink>
    </div>
    <div id ="map-canvas" style="width: 960px; height: 500px"></div>

</asp:Content>

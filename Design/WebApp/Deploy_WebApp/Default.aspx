﻿<%@ Page Title="Luggage Guys" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="LuggageWebApp._Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">                
                <h2>The world's first wireless luggage tracking system.</h2>
            </hgroup>
            <p>
                This project showcases in-baggage beacon tracking device which uses wireless connectivity to locate baggage on a global scale 
                through the use of embedded wireless communication systems, internet of things (IoT) cloud platform and social media infrastructure.
            </p>
            <p>
                With users able to track their luggage from their mobile phone and receive text messages confirming safe arrival, this technology 
                could help passengers fly with the knowledge that if their bag happens to be misdirected, they can help the airport to locate and 
                send it to the correct location.
            </p>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <section id="Overview">
        <h3>The project demonstrates the following:</h3>
        <ol class="round">
            <li class="one">
                <h5>Wireless Communication</h5>
                The ability of transmitting signal from a beacon device to the receiver reliably. 
            </li>
            <li class="two">
                <h5>Global Positioning System (GPS) Tracking</h5>
                With the use of GPS tracking device the project can demonstrate that luggage position can be recorded and reported to the base center wirelessly.
            </li>
            <li class="three">
                <h5>Real-time Google-Earth Location Mapping</h5>
                With the data constantly trasmitting back to the base center, positions of the luggages can be plotted in real-time on Google-Earth.
            </li>
        </ol>    
    </section>
    <section id="BurningPhoto">
        <h3>The Burning Platform</h3><br />
        <asp:Image ID="burningform" ImageUrl="~/Images/burningplatform.png" runat="server" />
    </section>

</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LugLock
{
    public partial class frmSystemConfiguration : Form
    {
        #region Private Variables

        private string strServer_IP = "";
        private int intServer_Port = 0;
        private string strClient_IP = "";
        private string strFrequency_Default = "1 Second";
        private string strGPS_File_Extension = "GPS Files|*.kml|GPS Files|*.kmz";
        private string strGPS_File_Path = "";
        private bool bolShow_Path_Default = false;

        #endregion


        #region Public Properties

        public string Server_IP
        {
            set
            {
                strServer_IP = value;
            }
            get
            {
                return strServer_IP;
            }
        }

        public int Server_Port
        {
            set
            {
                intServer_Port = value;
            }
            get
            {
                return intServer_Port;
            }
        }

        public string Client_IP
        {
            set
            {
                strClient_IP = value;
            }
            get
            {
                return strClient_IP;
            }
        }

        public string Frequency_Default
        {
            set
            {
                strFrequency_Default = value;
            }
            get
            {
                return strFrequency_Default;
            }
        }

        public string GPS_File_Extension
        {
            set
            {
                strGPS_File_Extension = value;
            }
            get
            {
                return strGPS_File_Extension;
            }
        }

        public string GPS_File_Path
        {
            set
            {
                strGPS_File_Path = value;
            }
            get
            {
                return strGPS_File_Path;
            }
        }

        public bool Show_Path_Default
        {
            set
            {
                bolShow_Path_Default = value;
            }
            get
            {
                return bolShow_Path_Default;
            }
        }

        #endregion


        #region Constructor

        public frmSystemConfiguration()
        {
            InitializeComponent();
            txtServerIP.ValidatingType = typeof(System.Net.IPAddress);
            txtClientIP.ValidatingType = typeof(System.Net.IPAddress);
        }

        #endregion


        #region Commands 

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            string strFilePath = "";
            openFileDialog1.FileName = "";
            openFileDialog1.ShowDialog();
            strFilePath = openFileDialog1.FileName;
            if (strFilePath != "")
            {
                txtOutputFilePath.Text = strFilePath;
                if (cboFrequency.SelectedIndex == -1)
                {
                    cboFrequency.SelectedIndex = 0;
                }
            }
            else
            {
                txtOutputFilePath.Text = "";
            }
        }

        private void btnSaveConfiguration_Click(object sender, EventArgs e)
        {
            Server_IP = txtServerIP.Text;
            Server_Port = Convert.ToInt16(txtPort.Text);
            Client_IP = txtClientIP.Text;
            Frequency_Default = cboFrequency.SelectedItem.ToString();
            strGPS_File_Path = txtOutputFilePath.Text;
            Show_Path_Default = chkShowPath.Checked;
            Close();
        }

        #endregion


        #region Public Method

        public void LoadConfiguration()
        {
            txtServerIP.Text = Server_IP;
            txtPort.Text = Server_Port.ToString();
            txtClientIP.Text = Client_IP;            
            openFileDialog1.DefaultExt = GPS_File_Extension;
            txtOutputFilePath.Text = GPS_File_Path;
            chkShowPath.Checked = Show_Path_Default;

            if (cboFrequency.Items.Contains(Frequency_Default))
                cboFrequency.SelectedIndex = cboFrequency.Items.IndexOf(Frequency_Default);
            else
                cboFrequency.SelectedIndex = 0;

        }

        #endregion
    }
}

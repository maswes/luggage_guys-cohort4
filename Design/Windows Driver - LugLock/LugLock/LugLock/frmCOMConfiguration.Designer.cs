﻿namespace LugLock
{
    partial class frmCOMConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpServer = new System.Windows.Forms.GroupBox();
            this.txtStopBit = new System.Windows.Forms.TextBox();
            this.lblStopBit = new System.Windows.Forms.Label();
            this.txtParity = new System.Windows.Forms.TextBox();
            this.lblParity = new System.Windows.Forms.Label();
            this.txtDataBits = new System.Windows.Forms.TextBox();
            this.txtBaudRate = new System.Windows.Forms.TextBox();
            this.lblDataBits = new System.Windows.Forms.Label();
            this.txtCOMPort = new System.Windows.Forms.MaskedTextBox();
            this.lblBaudRate = new System.Windows.Forms.Label();
            this.lblPort = new System.Windows.Forms.Label();
            this.btnSaveConfiguration = new System.Windows.Forms.Button();
            this.txtDataLength = new System.Windows.Forms.TextBox();
            this.lblLength = new System.Windows.Forms.Label();
            this.grpServer.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpServer
            // 
            this.grpServer.Controls.Add(this.txtDataLength);
            this.grpServer.Controls.Add(this.lblLength);
            this.grpServer.Controls.Add(this.txtStopBit);
            this.grpServer.Controls.Add(this.lblStopBit);
            this.grpServer.Controls.Add(this.txtParity);
            this.grpServer.Controls.Add(this.lblParity);
            this.grpServer.Controls.Add(this.txtDataBits);
            this.grpServer.Controls.Add(this.txtBaudRate);
            this.grpServer.Controls.Add(this.lblDataBits);
            this.grpServer.Controls.Add(this.txtCOMPort);
            this.grpServer.Controls.Add(this.lblBaudRate);
            this.grpServer.Controls.Add(this.lblPort);
            this.grpServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpServer.Location = new System.Drawing.Point(12, 12);
            this.grpServer.Name = "grpServer";
            this.grpServer.Size = new System.Drawing.Size(677, 233);
            this.grpServer.TabIndex = 1;
            this.grpServer.TabStop = false;
            this.grpServer.Text = "Server";
            // 
            // txtStopBit
            // 
            this.txtStopBit.Location = new System.Drawing.Point(113, 154);
            this.txtStopBit.Name = "txtStopBit";
            this.txtStopBit.Size = new System.Drawing.Size(100, 23);
            this.txtStopBit.TabIndex = 12;
            this.txtStopBit.Text = "1";
            // 
            // lblStopBit
            // 
            this.lblStopBit.AutoSize = true;
            this.lblStopBit.Location = new System.Drawing.Point(21, 154);
            this.lblStopBit.Name = "lblStopBit";
            this.lblStopBit.Size = new System.Drawing.Size(61, 17);
            this.lblStopBit.TabIndex = 11;
            this.lblStopBit.Text = "Stop Bit:";
            // 
            // txtParity
            // 
            this.txtParity.Location = new System.Drawing.Point(113, 125);
            this.txtParity.Name = "txtParity";
            this.txtParity.Size = new System.Drawing.Size(100, 23);
            this.txtParity.TabIndex = 10;
            this.txtParity.Text = "0";
            // 
            // lblParity
            // 
            this.lblParity.AutoSize = true;
            this.lblParity.Location = new System.Drawing.Point(21, 125);
            this.lblParity.Name = "lblParity";
            this.lblParity.Size = new System.Drawing.Size(48, 17);
            this.lblParity.TabIndex = 9;
            this.lblParity.Text = "Parity:";
            // 
            // txtDataBits
            // 
            this.txtDataBits.Location = new System.Drawing.Point(113, 96);
            this.txtDataBits.Name = "txtDataBits";
            this.txtDataBits.Size = new System.Drawing.Size(100, 23);
            this.txtDataBits.TabIndex = 8;
            // 
            // txtBaudRate
            // 
            this.txtBaudRate.Location = new System.Drawing.Point(113, 67);
            this.txtBaudRate.Name = "txtBaudRate";
            this.txtBaudRate.Size = new System.Drawing.Size(100, 23);
            this.txtBaudRate.TabIndex = 7;
            // 
            // lblDataBits
            // 
            this.lblDataBits.AutoSize = true;
            this.lblDataBits.Location = new System.Drawing.Point(21, 96);
            this.lblDataBits.Name = "lblDataBits";
            this.lblDataBits.Size = new System.Drawing.Size(69, 17);
            this.lblDataBits.TabIndex = 5;
            this.lblDataBits.Text = "Data Bits:";
            // 
            // txtCOMPort
            // 
            this.txtCOMPort.Location = new System.Drawing.Point(113, 37);
            this.txtCOMPort.Name = "txtCOMPort";
            this.txtCOMPort.Size = new System.Drawing.Size(558, 23);
            this.txtCOMPort.TabIndex = 3;
            // 
            // lblBaudRate
            // 
            this.lblBaudRate.AutoSize = true;
            this.lblBaudRate.Location = new System.Drawing.Point(21, 67);
            this.lblBaudRate.Name = "lblBaudRate";
            this.lblBaudRate.Size = new System.Drawing.Size(79, 17);
            this.lblBaudRate.TabIndex = 1;
            this.lblBaudRate.Text = "Baud Rate:";
            // 
            // lblPort
            // 
            this.lblPort.AutoSize = true;
            this.lblPort.Location = new System.Drawing.Point(21, 37);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(38, 17);
            this.lblPort.TabIndex = 0;
            this.lblPort.Text = "Port:";
            // 
            // btnSaveConfiguration
            // 
            this.btnSaveConfiguration.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveConfiguration.Location = new System.Drawing.Point(519, 492);
            this.btnSaveConfiguration.Name = "btnSaveConfiguration";
            this.btnSaveConfiguration.Size = new System.Drawing.Size(164, 42);
            this.btnSaveConfiguration.TabIndex = 13;
            this.btnSaveConfiguration.Text = "Save COM Configuration";
            this.btnSaveConfiguration.UseVisualStyleBackColor = true;
            this.btnSaveConfiguration.Click += new System.EventHandler(this.btnSaveConfiguration_Click);
            // 
            // txtDataLength
            // 
            this.txtDataLength.Location = new System.Drawing.Point(113, 183);
            this.txtDataLength.Name = "txtDataLength";
            this.txtDataLength.Size = new System.Drawing.Size(100, 23);
            this.txtDataLength.TabIndex = 14;
            this.txtDataLength.Text = "35";
            // 
            // lblLength
            // 
            this.lblLength.AutoSize = true;
            this.lblLength.Location = new System.Drawing.Point(21, 183);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(56, 17);
            this.lblLength.TabIndex = 13;
            this.lblLength.Text = "Length:";
            // 
            // frmCOMConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 546);
            this.Controls.Add(this.btnSaveConfiguration);
            this.Controls.Add(this.grpServer);
            this.Name = "frmCOMConfiguration";
            this.Text = "COM Configuration";
            this.grpServer.ResumeLayout(false);
            this.grpServer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpServer;
        private System.Windows.Forms.TextBox txtStopBit;
        private System.Windows.Forms.Label lblStopBit;
        private System.Windows.Forms.TextBox txtParity;
        private System.Windows.Forms.Label lblParity;
        private System.Windows.Forms.TextBox txtDataBits;
        private System.Windows.Forms.TextBox txtBaudRate;
        private System.Windows.Forms.Label lblDataBits;
        private System.Windows.Forms.MaskedTextBox txtCOMPort;
        private System.Windows.Forms.Label lblBaudRate;
        private System.Windows.Forms.Label lblPort;
        private System.Windows.Forms.Button btnSaveConfiguration;
        private System.Windows.Forms.TextBox txtDataLength;
        private System.Windows.Forms.Label lblLength;
    }
}
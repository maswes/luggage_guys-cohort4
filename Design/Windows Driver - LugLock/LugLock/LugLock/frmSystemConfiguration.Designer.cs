﻿namespace LugLock
{
    partial class frmSystemConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpServer = new System.Windows.Forms.GroupBox();
            this.lblServerIP = new System.Windows.Forms.Label();
            this.lblServerPort = new System.Windows.Forms.Label();
            this.txtServerIP = new System.Windows.Forms.MaskedTextBox();
            this.txtPort = new System.Windows.Forms.MaskedTextBox();
            this.grpClient = new System.Windows.Forms.GroupBox();
            this.txtClientIP = new System.Windows.Forms.MaskedTextBox();
            this.lblClientIP = new System.Windows.Forms.Label();
            this.grpGPS = new System.Windows.Forms.GroupBox();
            this.lblGPSOutputFile = new System.Windows.Forms.Label();
            this.txtOutputFilePath = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.lblFrequency = new System.Windows.Forms.Label();
            this.cboFrequency = new System.Windows.Forms.ComboBox();
            this.chkShowPath = new System.Windows.Forms.CheckBox();
            this.lblShowPath = new System.Windows.Forms.Label();
            this.btnSaveConfiguration = new System.Windows.Forms.Button();
            this.grpServer.SuspendLayout();
            this.grpClient.SuspendLayout();
            this.grpGPS.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpServer
            // 
            this.grpServer.Controls.Add(this.txtPort);
            this.grpServer.Controls.Add(this.txtServerIP);
            this.grpServer.Controls.Add(this.lblServerPort);
            this.grpServer.Controls.Add(this.lblServerIP);
            this.grpServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpServer.Location = new System.Drawing.Point(13, 13);
            this.grpServer.Name = "grpServer";
            this.grpServer.Size = new System.Drawing.Size(677, 100);
            this.grpServer.TabIndex = 0;
            this.grpServer.TabStop = false;
            this.grpServer.Text = "Server";
            // 
            // lblServerIP
            // 
            this.lblServerIP.AutoSize = true;
            this.lblServerIP.Location = new System.Drawing.Point(19, 30);
            this.lblServerIP.Name = "lblServerIP";
            this.lblServerIP.Size = new System.Drawing.Size(70, 17);
            this.lblServerIP.TabIndex = 0;
            this.lblServerIP.Text = "Server IP:";
            // 
            // lblServerPort
            // 
            this.lblServerPort.AutoSize = true;
            this.lblServerPort.Location = new System.Drawing.Point(19, 59);
            this.lblServerPort.Name = "lblServerPort";
            this.lblServerPort.Size = new System.Drawing.Size(84, 17);
            this.lblServerPort.TabIndex = 1;
            this.lblServerPort.Text = "Server Port:";
            // 
            // txtServerIP
            // 
            this.txtServerIP.Location = new System.Drawing.Point(111, 23);
            this.txtServerIP.Name = "txtServerIP";
            this.txtServerIP.Size = new System.Drawing.Size(110, 23);
            this.txtServerIP.TabIndex = 3;
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(111, 53);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(50, 23);
            this.txtPort.TabIndex = 4;
            // 
            // grpClient
            // 
            this.grpClient.Controls.Add(this.txtClientIP);
            this.grpClient.Controls.Add(this.lblClientIP);
            this.grpClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpClient.Location = new System.Drawing.Point(13, 119);
            this.grpClient.Name = "grpClient";
            this.grpClient.Size = new System.Drawing.Size(677, 100);
            this.grpClient.TabIndex = 1;
            this.grpClient.TabStop = false;
            this.grpClient.Text = "Client";
            // 
            // txtClientIP
            // 
            this.txtClientIP.Location = new System.Drawing.Point(111, 23);
            this.txtClientIP.Name = "txtClientIP";
            this.txtClientIP.Size = new System.Drawing.Size(110, 23);
            this.txtClientIP.TabIndex = 3;
            // 
            // lblClientIP
            // 
            this.lblClientIP.AutoSize = true;
            this.lblClientIP.Location = new System.Drawing.Point(19, 30);
            this.lblClientIP.Name = "lblClientIP";
            this.lblClientIP.Size = new System.Drawing.Size(63, 17);
            this.lblClientIP.TabIndex = 0;
            this.lblClientIP.Text = "Client IP:";
            // 
            // grpGPS
            // 
            this.grpGPS.Controls.Add(this.lblShowPath);
            this.grpGPS.Controls.Add(this.chkShowPath);
            this.grpGPS.Controls.Add(this.cboFrequency);
            this.grpGPS.Controls.Add(this.lblFrequency);
            this.grpGPS.Controls.Add(this.btnBrowse);
            this.grpGPS.Controls.Add(this.txtOutputFilePath);
            this.grpGPS.Controls.Add(this.lblGPSOutputFile);
            this.grpGPS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpGPS.Location = new System.Drawing.Point(13, 230);
            this.grpGPS.Name = "grpGPS";
            this.grpGPS.Size = new System.Drawing.Size(677, 259);
            this.grpGPS.TabIndex = 2;
            this.grpGPS.TabStop = false;
            this.grpGPS.Text = "GPS file generation";
            // 
            // lblGPSOutputFile
            // 
            this.lblGPSOutputFile.AutoSize = true;
            this.lblGPSOutputFile.Location = new System.Drawing.Point(19, 30);
            this.lblGPSOutputFile.Name = "lblGPSOutputFile";
            this.lblGPSOutputFile.Size = new System.Drawing.Size(81, 17);
            this.lblGPSOutputFile.TabIndex = 0;
            this.lblGPSOutputFile.Text = "Output File:";
            // 
            // txtOutputFilePath
            // 
            this.txtOutputFilePath.Location = new System.Drawing.Point(111, 30);
            this.txtOutputFilePath.Name = "txtOutputFilePath";
            this.txtOutputFilePath.ReadOnly = true;
            this.txtOutputFilePath.Size = new System.Drawing.Size(473, 23);
            this.txtOutputFilePath.TabIndex = 1;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "kml";
            this.openFileDialog1.Filter = "GPS Files|*.kml|GPS Files|*.kmz";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(591, 29);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 2;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // lblFrequency
            // 
            this.lblFrequency.AutoSize = true;
            this.lblFrequency.Location = new System.Drawing.Point(19, 69);
            this.lblFrequency.Name = "lblFrequency";
            this.lblFrequency.Size = new System.Drawing.Size(79, 17);
            this.lblFrequency.TabIndex = 3;
            this.lblFrequency.Text = "Frequency:";
            // 
            // cboFrequency
            // 
            this.cboFrequency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFrequency.FormattingEnabled = true;
            this.cboFrequency.Items.AddRange(new object[] {
            "1 second",
            "2 seconds",
            "3 seconds",
            "4 seconds",
            "5 seconds"});
            this.cboFrequency.Location = new System.Drawing.Point(111, 69);
            this.cboFrequency.Name = "cboFrequency";
            this.cboFrequency.Size = new System.Drawing.Size(121, 24);
            this.cboFrequency.TabIndex = 4;
            // 
            // chkShowPath
            // 
            this.chkShowPath.AutoSize = true;
            this.chkShowPath.Location = new System.Drawing.Point(111, 113);
            this.chkShowPath.Name = "chkShowPath";
            this.chkShowPath.Size = new System.Drawing.Size(15, 14);
            this.chkShowPath.TabIndex = 5;
            this.chkShowPath.UseVisualStyleBackColor = true;
            // 
            // lblShowPath
            // 
            this.lblShowPath.AutoSize = true;
            this.lblShowPath.Location = new System.Drawing.Point(19, 110);
            this.lblShowPath.Name = "lblShowPath";
            this.lblShowPath.Size = new System.Drawing.Size(79, 17);
            this.lblShowPath.TabIndex = 6;
            this.lblShowPath.Text = "Show Path:";
            // 
            // btnSaveConfiguration
            // 
            this.btnSaveConfiguration.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveConfiguration.Location = new System.Drawing.Point(526, 506);
            this.btnSaveConfiguration.Name = "btnSaveConfiguration";
            this.btnSaveConfiguration.Size = new System.Drawing.Size(164, 42);
            this.btnSaveConfiguration.TabIndex = 3;
            this.btnSaveConfiguration.Text = "Save IPConfiguration";
            this.btnSaveConfiguration.UseVisualStyleBackColor = true;
            this.btnSaveConfiguration.Click += new System.EventHandler(this.btnSaveConfiguration_Click);
            // 
            // SystemConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(702, 560);
            this.Controls.Add(this.btnSaveConfiguration);
            this.Controls.Add(this.grpGPS);
            this.Controls.Add(this.grpClient);
            this.Controls.Add(this.grpServer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SystemConfiguration";
            this.Text = "System IPConfiguration";
            this.grpServer.ResumeLayout(false);
            this.grpServer.PerformLayout();
            this.grpClient.ResumeLayout(false);
            this.grpClient.PerformLayout();
            this.grpGPS.ResumeLayout(false);
            this.grpGPS.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpServer;
        private System.Windows.Forms.Label lblServerPort;
        private System.Windows.Forms.Label lblServerIP;
        private System.Windows.Forms.MaskedTextBox txtPort;
        private System.Windows.Forms.MaskedTextBox txtServerIP;
        private System.Windows.Forms.GroupBox grpClient;
        private System.Windows.Forms.MaskedTextBox txtClientIP;
        private System.Windows.Forms.Label lblClientIP;
        private System.Windows.Forms.GroupBox grpGPS;
        private System.Windows.Forms.Label lblGPSOutputFile;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtOutputFilePath;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ComboBox cboFrequency;
        private System.Windows.Forms.Label lblFrequency;
        private System.Windows.Forms.Label lblShowPath;
        private System.Windows.Forms.CheckBox chkShowPath;
        private System.Windows.Forms.Button btnSaveConfiguration;
    }
}
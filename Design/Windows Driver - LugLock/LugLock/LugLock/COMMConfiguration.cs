﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LugLock
{
    class COMMConfiguration
    {
        #region Private Variables

        private string strCommPort = "COM5";
        private int intBaudRate = 115200;
        private int intDataBits = 8;
        private int intDataLength = 35;
        private int intParity = 0;
        private int intStopBit = 1;

        #endregion

        #region Public Properties

        public string CommPort
        {
            set
            {
                strCommPort = value;
            }
            get
            {
                return strCommPort;
            }
        }

        public int BaudRate
        {
            set
            {
                intBaudRate = value;
            }
            get
            {
                return intBaudRate;
            }
        }

        public int DataBits
        {
            set
            {
                intDataBits = value;
            }
            get
            {
                return intDataBits;
            }
        }

        public int DataLength
        {
            set
            {
                intDataLength = value;
            }
            get
            {
                return intDataLength;
            }
        }

        public int Parity
        {
            set
            {
                intParity = value;
            }
            get
            {
                return intParity;
            }
        }

        public int StopBit
        {
            set
            {
                intStopBit = value;
            }
            get
            {
                return intStopBit;
            }
        }

        #endregion
    }
}

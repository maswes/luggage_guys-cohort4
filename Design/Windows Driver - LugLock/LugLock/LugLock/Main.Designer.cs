﻿namespace LugLock
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.COMConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.systemConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.webServiceTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.localHostServiceTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.localServiceTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startCOMMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startIPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtLogConsole = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.runToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1203, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.COMConfigurationToolStripMenuItem,
            this.systemConfigurationToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // COMConfigurationToolStripMenuItem
            // 
            this.COMConfigurationToolStripMenuItem.Name = "COMConfigurationToolStripMenuItem";
            this.COMConfigurationToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.COMConfigurationToolStripMenuItem.Text = "&COM Configuration";
            this.COMConfigurationToolStripMenuItem.Click += new System.EventHandler(this.COMConfigurationToolStripMenuItem_Click);
            // 
            // systemConfigurationToolStripMenuItem
            // 
            this.systemConfigurationToolStripMenuItem.Name = "systemConfigurationToolStripMenuItem";
            this.systemConfigurationToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.systemConfigurationToolStripMenuItem.Text = "&IP Configuration";
            this.systemConfigurationToolStripMenuItem.Click += new System.EventHandler(this.systemConfigurationToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(176, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // runToolStripMenuItem
            // 
            this.runToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.webServiceTestToolStripMenuItem,
            this.localHostServiceTestToolStripMenuItem,
            this.localServiceTestToolStripMenuItem,
            this.startCOMMToolStripMenuItem,
            this.startIPToolStripMenuItem,
            this.stopToolStripMenuItem});
            this.runToolStripMenuItem.Name = "runToolStripMenuItem";
            this.runToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.runToolStripMenuItem.Text = "Run";
            // 
            // webServiceTestToolStripMenuItem
            // 
            this.webServiceTestToolStripMenuItem.Name = "webServiceTestToolStripMenuItem";
            this.webServiceTestToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.webServiceTestToolStripMenuItem.Text = "Web Service &Test";
            this.webServiceTestToolStripMenuItem.Click += new System.EventHandler(this.webServiceTestToolStripMenuItem_Click);
            // 
            // localHostServiceTestToolStripMenuItem
            // 
            this.localHostServiceTestToolStripMenuItem.Name = "localHostServiceTestToolStripMenuItem";
            this.localHostServiceTestToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.localHostServiceTestToolStripMenuItem.Text = "Local Host Service Test";
            this.localHostServiceTestToolStripMenuItem.Click += new System.EventHandler(this.localHostServiceTestToolStripMenuItem_Click);
            // 
            // localServiceTestToolStripMenuItem
            // 
            this.localServiceTestToolStripMenuItem.Name = "localServiceTestToolStripMenuItem";
            this.localServiceTestToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.localServiceTestToolStripMenuItem.Text = "Local &Service Test";
            this.localServiceTestToolStripMenuItem.Click += new System.EventHandler(this.localServiceTestToolStripMenuItem_Click);
            // 
            // startCOMMToolStripMenuItem
            // 
            this.startCOMMToolStripMenuItem.Name = "startCOMMToolStripMenuItem";
            this.startCOMMToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.startCOMMToolStripMenuItem.Text = "Start &Comm";
            this.startCOMMToolStripMenuItem.Click += new System.EventHandler(this.startCOMMToolStripMenuItem_Click);
            // 
            // startIPToolStripMenuItem
            // 
            this.startIPToolStripMenuItem.Name = "startIPToolStripMenuItem";
            this.startIPToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.startIPToolStripMenuItem.Text = "Start &IP";
            this.startIPToolStripMenuItem.Click += new System.EventHandler(this.startToolStripMenuItem_Click);
            // 
            // stopToolStripMenuItem
            // 
            this.stopToolStripMenuItem.Enabled = false;
            this.stopToolStripMenuItem.Name = "stopToolStripMenuItem";
            this.stopToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.stopToolStripMenuItem.Text = "St&op";
            this.stopToolStripMenuItem.Click += new System.EventHandler(this.stopToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // txtLogConsole
            // 
            this.txtLogConsole.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLogConsole.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLogConsole.Location = new System.Drawing.Point(0, 24);
            this.txtLogConsole.Multiline = true;
            this.txtLogConsole.Name = "txtLogConsole";
            this.txtLogConsole.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLogConsole.Size = new System.Drawing.Size(1203, 713);
            this.txtLogConsole.TabIndex = 1;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1203, 737);
            this.Controls.Add(this.txtLogConsole);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.Text = "LugLock Tracker";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem systemConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startIPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.TextBox txtLogConsole;
        private System.Windows.Forms.ToolStripMenuItem COMConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startCOMMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem webServiceTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem localServiceTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem localHostServiceTestToolStripMenuItem;
    }
}


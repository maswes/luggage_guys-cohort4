﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LugLock
{
    class IPConfiguration
    {
        #region Private Variables

        private string strServer_IP = "";
        private int intServer_Port = 0;
        private string strClient_IP = "";
        private string strFrequency_Default = "1 Second";
        private string strGPS_File_Extension = "GPS Files|*.kml|GPS Files|*.kmz";
        private string strOutputFilePath = "";
        private bool bolShow_Path_Default = false;

        #endregion

        #region Public Properties

        public string Server_IP
        {
            set
            {
                strServer_IP = value;
            }
            get
            {
                return strServer_IP;
            }
        }

        public int Server_Port
        {
            set
            {
                intServer_Port = value;
            }
            get
            {
                return intServer_Port;
            }
        }

        public string Client_IP
        {
            set
            {
                strClient_IP = value;
            }
            get
            {
                return strClient_IP;
            }
        }

        public string Frequency_Default
        {
            set
            {
                strFrequency_Default = value;
            }
            get
            {
                return strFrequency_Default;
            }
        }

        public string GPS_File_Extension
        {
            set
            {
                strGPS_File_Extension = value;
            }
            get
            {
                return strGPS_File_Extension;
            }
        }

        public string Output_File_Path
        {
            set
            {
                strOutputFilePath = value;
            }
            get
            {
                return strOutputFilePath;
            }
        }

        public bool Show_Path_Default
        {
            set
            {
                bolShow_Path_Default = value;
            }
            get
            {
                return bolShow_Path_Default;
            }
        }

        #endregion
    }
}

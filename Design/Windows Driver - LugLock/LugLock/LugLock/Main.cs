﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Configuration;
using LugLock.GeoDataAPIService;
using LugLock.LocalHostGeoAPI;

namespace LugLock
{
    public partial class Main : Form
    {
        #region Private Variables

        struct Beacon
        {
            public string UID;
            public string IP;
            public string Lat;
            public string Long;           
        };
        private bool shutdown = false;
        private IPConfiguration ipConfig = new IPConfiguration();
        private COMMConfiguration commConfig = new COMMConfiguration();
        private static TcpListener listener;                                //TCP/IP Listener
        private const int LIMIT = 1;                                        //1 concurrent client
        System.IO.Ports.SerialPort portCommSerial = null;
        StreamReader srClient1 = null;
        StreamWriter swClient1 = null;
        Thread threadIP = null;
        Thread threadCOMM = null;
        Socket soc = null;
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        private int freqCounter = 0;
        private LugLock.GeoDataAPIService.GeoData postedGeoData;

        private bool bolProcessStartIP = false;
        private bool bolProcessStartCOMM = false;
        private Beacon currentBeacon = new Beacon();

        // Crc Computation Class
        private static CRCTool compCRC = new CRCTool();

        #endregion


        #region Constructor

        public Main()
        {
            InitializeComponent();
            LoadAppConfiguration();
        }

        #endregion


        #region Private Methods

        private void LoadAppConfiguration()
        {
            if (ConfigurationManager.AppSettings["server_ip"] != null)
            {
                ipConfig.Server_IP = ConfigurationManager.AppSettings["server_ip"].ToString();
            }
            if (ConfigurationManager.AppSettings["server_port"] != null)
            {
                ipConfig.Server_Port = Convert.ToInt16(ConfigurationManager.AppSettings["server_port"].ToString());
            }
            if (ConfigurationManager.AppSettings["client_ip"] != null)
            {
                ipConfig.Client_IP = ConfigurationManager.AppSettings["client_ip"].ToString();
            }
            if (ConfigurationManager.AppSettings["frequency_default"] != null)
            {
                ipConfig.Frequency_Default = ConfigurationManager.AppSettings["frequency_default"].ToString();
                SetTimer(ipConfig.Frequency_Default);
            }
            if (ConfigurationManager.AppSettings["show_path_default"] != null)
            {
                if (ConfigurationManager.AppSettings["show_path_default"].ToString().ToLower() == "false")
                {
                    ipConfig.Show_Path_Default = false;
                }
                else
                {
                    ipConfig.Show_Path_Default = true;
                }
            }
            if (ConfigurationManager.AppSettings["gps_file_extension"] != null)
            {
                ipConfig.GPS_File_Extension = ConfigurationManager.AppSettings["gps_file_extension"].ToString();
            }

            //Set default data for beacon object
            currentBeacon.UID = "Cohort4";
            currentBeacon.Lat = "";
            currentBeacon.Long = "";
        }

        /// <summary>
        /// Start the server application
        /// </summary>
        private void StartServerProcess()
        {
            //Setup server listener
            IPAddress ip = IPAddress.Parse(ipConfig.Server_IP);

            listener = new TcpListener(ip, ipConfig.Server_Port);
            listener.Start();

            threadIP = new Thread(new ThreadStart(IPService));
            threadIP.Start();
        }

        /// <summary>
        /// Start the server application
        /// </summary>
        private void StartCOMMServerProcess()
        {
            shutdown = false;
            threadIP = new Thread(new ThreadStart(COMMService));
            threadIP.Start();
        }

        /// <summary>
        /// Client service thread: This method handles the communication, 
        /// data transfer and receive, and update global map
        /// </summary>
        private void IPService()
        {
            string myIP = "";
            string endPoint = "";
            string[] endPointValues;

            Action action;

            while (true)
            {
                soc = null;

                try
                {
                    soc = listener.AcceptSocket();
                    Stream s = new NetworkStream(soc);
                    srClient1 = new StreamReader(s);
                    swClient1 = new StreamWriter(s);

                    //Display to console
                    action = () => this.txtLogConsole.Text += String.Format("Connected: {0}\r\n", soc.RemoteEndPoint);
                    this.txtLogConsole.Invoke(action, null);

                    //Parse IP address
                    endPoint = soc.RemoteEndPoint.ToString();
                    if (endPoint != "")
                    {
                        endPointValues = endPoint.Split(':');
                        if (endPointValues.Length > 0)
                            myIP = endPointValues[0];
                    }

                    //Check to see if the client matches
                    if (myIP == ipConfig.Client_IP)
                    {
                        //timer.Start();
                        swClient1.AutoFlush = true; // enable automatic flushing
                        while (true)
                        {
                            string data = srClient1.ReadLine();
                            if (data == "" || data == null) break;

                            if (data != "")
                            {
                                currentBeacon.UID = data;
                                currentBeacon.IP = myIP;
                                currentBeacon.Lat = "";
                                currentBeacon.Long = "";
                            }
                            Thread.Sleep(1000 * freqCounter);

                            //Display to console
                            action = () => this.txtLogConsole.Text += String.Format("UID: {0}\tLat: {1}\tLong: {2}\r\n", currentBeacon.UID, currentBeacon.Lat, currentBeacon.Long);
                            this.txtLogConsole.Invoke(action, null);
                        }                        
                    }

                    s.Close();

                    if (soc != null)
                    {
                        //Display to console
                        action = () => this.txtLogConsole.Text += "\r\n" + String.Format("Disconnected: {0}", soc.RemoteEndPoint);
                        this.txtLogConsole.Invoke(action, null);

                        soc.Close();
                    }
                }
                catch (Exception e)
                {
                    //if (bolFormClosing == false)
                    //{
                        //Display to console                        
                        //action = () => this.txtLogConsole.Text += "\r\n" + String.Format("{0} [{1}]: {2}", myName, myIP, e.Message);
                        //this.txtLogConsole.Invoke(action, null);
                        if (soc != null)
                        {
                            soc.Close();
                            soc = null;
                        }
                    //}
                }
            }
        }

        /// <summary>
        /// Client service thread: This method handles the serial communication
        /// </summary>
        private void COMMService()
        {
            Action action;
            var receivedMsg = new byte[1];
            Beacon beacon = new Beacon();

            try
            {
                //Open Serial Port
                portCommSerial = new System.IO.Ports.SerialPort(commConfig.CommPort, commConfig.BaudRate, System.IO.Ports.Parity.None, commConfig.DataBits, System.IO.Ports.StopBits.One);
                portCommSerial.DtrEnable = true;
                portCommSerial.RtsEnable = true;
                portCommSerial.ReadTimeout = 3000;

                //Display to console
                action = () => this.txtLogConsole.Text += String.Format("Connected: {0}\r\n", commConfig.CommPort);
                this.txtLogConsole.Invoke(action, null);

                if (!portCommSerial.IsOpen)
                {
                    portCommSerial.Open();
                }

                //Read forever / until stop
                //4\nCohort4\nCohort4\nCohort4\nCohort4\nC
                while (!shutdown && bolProcessStartCOMM)
                {
                    string data = "";
                    string[] tokens;
                    string line = "";

                    //portCommSerial.Read(receivedMsg, 0, commConfig.DataLength);
                    while (data != "\n")
                    {
                        portCommSerial.Read(receivedMsg, 0, 1);
                        data = System.Text.Encoding.Default.GetString(receivedMsg);
                        if (data != "\n")
                            line += data;
                    }

                    tokens = line.Split(',');
                    if (tokens.Length == 3 && tokens[0].Length == 11 && tokens[1].Length == 10 && tokens[2].Length == 11)
                    {
                        beacon.UID = tokens[0].Trim().Replace("\0", "");
                        beacon.Lat = tokens[1].Trim();
                        beacon.Long = tokens[2].Trim();
                        if (beacon.Lat != "" && beacon.Long != "")
                        {
                            //Write to web service
                            ProcessGeoData(beacon);

                            //Display to console
                            action = () => this.txtLogConsole.Text += String.Format("UID: {0}\tLat: {1}\tLong: {2}\r\n", beacon.UID, beacon.Lat, beacon.Long);
                            this.txtLogConsole.Invoke(action, null);
                        }
                    }
                    //Thread.Sleep(1000 * freqCounter);
                }

                if (portCommSerial != null)
                {
                    //Display to console
                    action = () => this.txtLogConsole.Text += "\r\n" + String.Format("Disconnected: {0}", commConfig.CommPort);
                    this.txtLogConsole.Invoke(action, null);

                    portCommSerial.Close();
                }
            }
            catch (Exception ex)
            {
                if (portCommSerial != null)
                {
                    portCommSerial.Close();
                    portCommSerial = null;
                }
                //Display to console
                if (!shutdown)
                {
                    action = () => this.txtLogConsole.Text = String.Format("Error: {0}", ex.Message) + "\r\n" + this.txtLogConsole.Text;
                    this.txtLogConsole.Invoke(action, null);
                }
            }

        }

        private string ReadFromSerial()
        {
            try
            {
                portCommSerial = new System.IO.Ports.SerialPort(commConfig.CommPort, commConfig.BaudRate, System.IO.Ports.Parity.None, commConfig.DataBits, System.IO.Ports.StopBits.One);
                portCommSerial.DtrEnable = true;
                portCommSerial.RtsEnable = true;
                portCommSerial.ReadTimeout = 3000;

                var MessageBufferRequest = new byte[8] { 1, 3, 0, 28, 0, 1, 69, 204 };
                var MessageBufferReply = new byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 };
                int BufferLength = 8;

                if (!portCommSerial.IsOpen)
                {
                    portCommSerial.Open();
                }

                try
                {
                    portCommSerial.Write(MessageBufferRequest, 0, BufferLength);
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }

                System.Threading.Thread.Sleep(100);
                portCommSerial.Read(MessageBufferReply, 0, 7);
                return MessageBufferReply[3].ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        /// <summary>
        /// Set timer
        /// </summary>
        private void SetTimer(string freq)
        {
            switch (freq)
            {
                case "1 second":
                    freqCounter = 1;
                    break;
                case "2 seconds":
                    freqCounter = 2;
                    break;
                case "3 seconds":
                    freqCounter = 3;
                    break;
                case "4 seconds":
                    freqCounter = 4;
                    break;
                case "5 seconds":
                    freqCounter = 5;
                    break;
                default:
                    freqCounter = 1;
                    break;
            }
        }

        private void PlayBack(string strFilePath)
        {
            Beacon beacon = new Beacon();
            string[] tokens, nodes;

            if (File.Exists(strFilePath))
            {
                using (StreamReader reader = new StreamReader(strFilePath))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        line = reader.ReadLine();
                        if (line != null)
                        {
                            tokens = line.Split('\t');
                            if (tokens.Length == 3)
                            {
                                nodes = tokens[0].Split(':');
                                if (nodes.Length == 2)
                                    beacon.UID = nodes[1].Trim();

                                nodes = tokens[1].Split(':');
                                if (nodes.Length == 2)
                                    beacon.Lat = nodes[1].Trim();

                                nodes = tokens[2].Split(':');
                                if (nodes.Length == 2)
                                    beacon.Long = nodes[1].Trim();

                                if (beacon.Lat != "" && beacon.Long != "")
                                {
                                    ProcessGeoData(beacon);
                                }
                            }
                        }
                    }
                    reader.Close();
                }
            }
        }


        private void ProcessGeoData(Beacon beacon)
        {
            try
            {
                //currentBeacon.UID = "Cohort4";
                //currentBeacon.Lat = "3252.9953N";
                //currentBeacon.Long = "11714.2425W";

                LugLock.GeoDataAPIService.GeoDatatAPIService wsGeo = new LugLock.GeoDataAPIService.GeoDatatAPIService();
                int i = 0;
                bool Stop = false;
                //while(!Stop)
                //{
                    postedGeoData = new LugLock.GeoDataAPIService.GeoData();
                    postedGeoData.RecordedDate = DateTime.Now;
                    postedGeoData.RecordedTime = DateTime.Now;
                    postedGeoData.UID = beacon.UID;                
                    postedGeoData.Latitude = beacon.Lat.Substring(0,beacon.Lat.Length-1);
                    postedGeoData.LATDirection = beacon.Lat.Substring(beacon.Lat.Length - 1,1);
                    postedGeoData.Longtitude = beacon.Long.Substring(0, beacon.Long.Length - 1);
                    postedGeoData.LONGDirection = beacon.Long.Substring(beacon.Long.Length - 1, 1);

                    wsGeo.PostGeoData(postedGeoData.RecordedDate, true, postedGeoData.RecordedTime, true,
                        postedGeoData.UID, postedGeoData.Latitude, postedGeoData.LATDirection, postedGeoData.Longtitude, postedGeoData.LONGDirection);

                    i++;
                    //System.Threading.Thread.Sleep(1000);
                //}
            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
        }

        private void ProcessLocalGeoData()
        {
            try
            {
                MyWCFServices.GeoDatatAPIService wsGeo = new MyWCFServices.GeoDatatAPIService();
                int i = 0;
                bool Stop = false;
                while (!Stop)
                {
                    postedGeoData = new LugLock.GeoDataAPIService.GeoData();
                    postedGeoData.RecordedDate = DateTime.Now;
                    postedGeoData.RecordedTime = DateTime.Now;
                    postedGeoData.UID = currentBeacon.UID;
                    postedGeoData.Latitude = currentBeacon.Lat.Substring(0, currentBeacon.Lat.Length - 1);
                    postedGeoData.LATDirection = currentBeacon.Lat.Substring(currentBeacon.Lat.Length - 1, 1);
                    postedGeoData.Longtitude = currentBeacon.Long.Substring(0, currentBeacon.Long.Length - 1);
                    postedGeoData.LONGDirection = currentBeacon.Long.Substring(currentBeacon.Long.Length - 1, 1);

                    wsGeo.PostGeoData(postedGeoData.RecordedDate, postedGeoData.RecordedTime, 
                        postedGeoData.UID, postedGeoData.Latitude, postedGeoData.LATDirection, postedGeoData.Longtitude, postedGeoData.LONGDirection);

                    i++;
                    System.Threading.Thread.Sleep(1000);
                }
            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
        }

        private void ProcessLocalHostGeoData()
        {
            try
            {
                currentBeacon.UID = "Cohort4";
                currentBeacon.Lat = "3252.9953N";
                currentBeacon.Long = "11714.2425W";

                LugLock.LocalHostGeoAPI.GeoDatatAPIService wsGeo = new LocalHostGeoAPI.GeoDatatAPIService();
                int i = 0;
                bool Stop = false;
                while (!Stop)
                {
                    postedGeoData = new LugLock.GeoDataAPIService.GeoData();
                    postedGeoData.RecordedDate = DateTime.Now;
                    postedGeoData.RecordedTime = DateTime.Now;
                    postedGeoData.UID = currentBeacon.UID;
                    postedGeoData.Latitude = currentBeacon.Lat.Substring(0, currentBeacon.Lat.Length - 1);
                    postedGeoData.LATDirection = currentBeacon.Lat.Substring(currentBeacon.Lat.Length - 1, 1);
                    postedGeoData.Longtitude = currentBeacon.Long.Substring(0, currentBeacon.Long.Length - 1);
                    postedGeoData.LONGDirection = currentBeacon.Long.Substring(currentBeacon.Long.Length - 1, 1);

                    wsGeo.PostGeoData(postedGeoData.RecordedDate, true, postedGeoData.RecordedTime, true,
                        postedGeoData.UID, postedGeoData.Latitude, postedGeoData.LATDirection, postedGeoData.Longtitude, postedGeoData.LONGDirection);

                    i++;
                    System.Threading.Thread.Sleep(1000);
                }
            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
        }

        #endregion


        #region Commands

        private void systemConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSystemConfiguration frm = new frmSystemConfiguration();
            
            frm.Server_IP = ipConfig.Server_IP;
            frm.Server_Port = ipConfig.Server_Port;
            frm.Client_IP = ipConfig.Client_IP;
            frm.GPS_File_Path = ipConfig.Output_File_Path;
            frm.GPS_File_Extension = ipConfig.GPS_File_Extension;
            frm.Frequency_Default = ipConfig.Frequency_Default;
            frm.Show_Path_Default = ipConfig.Show_Path_Default;
            frm.LoadConfiguration();

            frm.ShowDialog();

            ipConfig.Server_IP = frm.Server_IP;
            ipConfig.Server_Port = frm.Server_Port;
            ipConfig.Client_IP = frm.Client_IP;
            ipConfig.Output_File_Path = frm.GPS_File_Path;
            ipConfig.Frequency_Default = frm.Frequency_Default;
            ipConfig.Show_Path_Default = frm.Show_Path_Default;
            SetTimer(ipConfig.Frequency_Default);
            
        }

        private void COMConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCOMConfiguration frm = new frmCOMConfiguration();

            frm.CommPort = commConfig.CommPort;
            frm.BaudRate = commConfig.BaudRate;
            frm.DataBits = commConfig.DataBits;
            frm.DataLength = commConfig.DataLength;
            frm.Parity = commConfig.Parity;
            frm.StopBit = commConfig.StopBit;

            frm.LoadConfiguration();

            frm.ShowDialog();

            commConfig.CommPort = frm.CommPort;
            commConfig.BaudRate = frm.BaudRate;
            commConfig.DataBits = frm.DataBits;
            commConfig.DataLength = frm.DataLength;
            commConfig.Parity = frm.Parity;
            commConfig.StopBit = frm.StopBit;
            SetTimer(ipConfig.Frequency_Default);
        }        
        
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //Start server process
                StartServerProcess();

                startIPToolStripMenuItem.Enabled = false;
                stopToolStripMenuItem.Enabled = true;
                systemConfigurationToolStripMenuItem.Enabled = false;
                bolProcessStartIP = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                startIPToolStripMenuItem.Enabled = true;
                stopToolStripMenuItem.Enabled = false;
            }
        }

        private void startCOMMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //Start server process
                StartCOMMServerProcess();

                startCOMMToolStripMenuItem.Enabled = false;
                stopToolStripMenuItem.Enabled = true;
                systemConfigurationToolStripMenuItem.Enabled = false;
                COMConfigurationToolStripMenuItem.Enabled = false;
                bolProcessStartCOMM = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                startIPToolStripMenuItem.Enabled = true;
                stopToolStripMenuItem.Enabled = false;
            }
        }
        
        private void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Action action;

            try
            {
                if (threadIP != null && bolProcessStartIP == true)
                {
                    listener.Stop();
                    listener = null;
                    if (soc != null)
                    {
                        soc.Close();
                        soc = null;
                    }
                    threadIP.Abort();
                    threadIP = null;
                    stopToolStripMenuItem.Enabled = false;
                    startIPToolStripMenuItem.Enabled = true;
                    systemConfigurationToolStripMenuItem.Enabled = true;
                    bolProcessStartIP = false;
                    timer.Stop();
                }
                else if (threadCOMM != null && bolProcessStartCOMM == true)
                {
                    if (portCommSerial != null)
                    {
                        //Display to console
                        action = () => this.txtLogConsole.Text += "\r\n" + String.Format("Disconnected: {0}", commConfig.CommPort);
                        this.txtLogConsole.Invoke(action, null);

                        portCommSerial.Close();
                    }

                    threadCOMM.Abort();
                    threadCOMM = null;
                    stopToolStripMenuItem.Enabled = false;
                    startIPToolStripMenuItem.Enabled = true;
                    startCOMMToolStripMenuItem.Enabled = true;
                    systemConfigurationToolStripMenuItem.Enabled = true;
                    COMConfigurationToolStripMenuItem.Enabled = true;
                    bolProcessStartCOMM = false;
                    timer.Stop();
                    shutdown = true;
                }
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                threadIP = null;
                timer.Stop();
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        #endregion

        private void webServiceTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Action action;
            string strFilePath = "C:\\Businesses\\UCSD\\WES\\Capstone\\Research\\Design\\LugLock\\LugLock\\LugLock\\uart.txt";
            //Process Playback
            strFilePath = Application.StartupPath + "\\uart.txt";

            action = () => this.txtLogConsole.Text = String.Format("Path: {0}", strFilePath) + "\r\n" + this.txtLogConsole.Text;
            this.txtLogConsole.Invoke(action, null);

            PlayBack(strFilePath);
        }

        private void localServiceTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessLocalGeoData();
        }

        /// <summary>
        /// Process local host web service GEO data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void localHostServiceTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessLocalHostGeoData();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LugLock
{
    public partial class frmCOMConfiguration : Form
    {
        #region Private Variables

        private string strCommPort = "COM7";
        private int intBaudRate = 115200;
        private int intDataBits = 8;
        private int intDataLength = 35;
        private int intParity = 0;
        private int intStopBit = 1;

        #endregion


        #region Public Properties

        public string CommPort
        {
            set
            {
                strCommPort = value;
            }
            get
            {
                return strCommPort;
            }
        }

        public int BaudRate
        {
            set
            {
                intBaudRate = value;
            }
            get
            {
                return intBaudRate;
            }
        }

        public int DataBits
        {
            set
            {
                intDataBits = value;
            }
            get
            {
                return intDataBits;
            }
        }

        public int DataLength
        {
            set
            {
                intDataLength = value;
            }
            get
            {
                return intDataLength;
            }
        }

        public int Parity
        {
            set
            {
                intParity = value;
            }
            get
            {
                return intParity;
            }
        }

        public int StopBit
        {
            set
            {
                intStopBit = value;
            }
            get
            {
                return intStopBit;
            }
        }

        #endregion


        #region Constructor

        public frmCOMConfiguration()
        {
            InitializeComponent();
        }

        #endregion


        #region Commands

        private void btnSaveConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                CommPort = txtCOMPort.Text;
                BaudRate = Convert.ToInt32(txtBaudRate.Text);
                DataBits = Convert.ToInt16(txtDataBits.Text);
                DataLength = Convert.ToInt16(txtDataLength.Text);
                Parity = Convert.ToInt16(txtParity.Text);
                StopBit = Convert.ToInt16(txtStopBit.Text);

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion


        #region Public Method

        public void LoadConfiguration()
        {
            this.txtCOMPort.Text = CommPort;
            this.txtBaudRate.Text = BaudRate.ToString();
            this.txtDataBits.Text = DataBits.ToString();
            this.txtParity.Text = Parity.ToString();
            this.txtStopBit.Text = StopBit.ToString();
        }

        #endregion
    }
}

using System;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Text;
using LoMaN.IO;
using Communication.IO.Tools;


namespace TestSerialCommunication
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	/// 

    class Class1
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 

        // The main serial stream
        static SerialStream ss;

        /** 
        * scrambler lookup table for fast computation. 
        */ 
        
        /// <summary>
        /// CRC parameters (default values are for CRC-32):
        /// </summary>
       
        static int   order      = 16;
        static ulong polynom    = 0x1021;
        static int   direct     = 1;
        static ulong crcinit    = 0xFFFF;
        static ulong crcxor     = 0x0;
        static int   refin      = 0;
        static int   refout     = 0;


        // internal global values:

        private static ulong crcmask;
        private static ulong crchighbit;
        private static ulong crcinit_direct;
        private static ulong crcinit_nondirect;
        private static ulong [] crctab = new ulong[256];

        private static CRCTool compCRC = new CRCTool();

        [STAThread]
        static void Main(string[] args)
        {
            //
            // TODO: Add code to start application here
            //
            // Create a serial port

            // at first, compute constant bit masks for whole CRC and CRC high bit
            
            crcmask = ((((ulong)1<<(order-1))-1)<<1)|1;
	        crchighbit = (ulong)1<<(order-1);

            // generate lookup table
            generate_crc_table();

            ulong bit, crc;
            int i;
            if ( direct == 0 ) 
            {
                crcinit_nondirect = crcinit;
                crc = crcinit;
                for (i=0; i<order; i++) 
                {

                    bit = crc & crchighbit;
                    crc<<= 1;
                    if ( bit != 0 ) 
                    {
                        crc^= polynom;
                    }
                }
                crc&= crcmask;
                crcinit_direct = crc;
            }
            else 
            {

                crcinit_direct = crcinit;
                crc = crcinit;
                for (i=0; i<order; i++) 
                {

                    bit = crc & 1;
                    if (bit != 0) 
                    {
                        crc^= polynom;
                    }
                    crc >>= 1;
                    if (bit != 0) 
                    {
                        crc|= crchighbit;
                    }
                }	
                crcinit_nondirect = crc;
            }

            // end seeding the CRC table
            ss = new SerialStream();
            try 
            {  
                // Make sure to use the right string,
                // this is directly parsed to the CreateFile
                // "\\\\.\\COM3",
                ss.Open("\\\\.\\COM3");
            }
            catch (Exception e) 
            {
                Console.WriteLine("Error: " + e.Message);
                return;
            }

            // Set port settings
            ss.SetPortSettings( 
                38400, 
                SerialStream.FlowControl.XOnXOff, 
                SerialStream.Parity.None, 
                8, 
                SerialStream.StopBits.One);
            
            // Set timeout so read ends after 20ms of silence after a response
            ss.SetTimeouts(100, 0, 0, 0, 0);

            // Create the StreamWriter used to send commands
            StreamWriter sw = new StreamWriter(ss, System.Text.Encoding.ASCII);

            // Create the Thread used to read responses
            Thread responseReaderThread = new Thread(new ThreadStart(ReadResponseThread));
            responseReaderThread.Start();

            // Read all returned lines
            for (;;) 
            {
                // Read command from console
                string command = Console.ReadLine();

                // Check for exit command
                if (command.Trim().ToLower() == "exit") 
                {
                    responseReaderThread.Abort();
                    break;
                }
                
                // Write command to port
                byte[] rawBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(command);
                
                // Different ways to calculate the CRC value
                //ushort usCrc16  = CalcCRCITT(rawBytes);

                compCRC.Init(CRCTool.CRCCode.CRC_CCITT);
                ushort usCrc16 = (ushort)compCRC.crctablefast(rawBytes);
                //usCrc16 = (ushort)crcbitbybit       (rawBytes, rawBytes.Length);
	            //usCrc16 = (ushort)crcbitbybitfast   (rawBytes, rawBytes.Length);
	            //usCrc16 = (ushort)crctable          (rawBytes, rawBytes.Length);
	            //usCrc16 = (ushort)crctablefast      (rawBytes, rawBytes.Length);

                string sMessageString = String.Format("!{0}|{1:X4}\r",command,usCrc16);
                //sprintf( message,"!%s|%04X\r", buf, crc );
                sw.WriteLine(sMessageString );
                sw.Flush();
            }
        }
        // Main loop for reading responses
        static void ReadResponseThread() 
        {
            StreamReader sr = new StreamReader(ss, System.Text.Encoding.ASCII);
            try 
            {
                for (;;) 
                {
                    // Read response from modem
                    string response = sr.ReadLine();
                    Console.WriteLine("Response: " + response);
                }
            }
            catch (ThreadAbortException) 
            {
            }
        }

        static ushort CalcCRCITT(byte[] DataStream)
        {
            uint uiCRCITTSum = 0xFFFF;
            uint uiByteValue;

            for (int iBufferIndex = 0; iBufferIndex < DataStream.Length; iBufferIndex++)
            {
                uiByteValue = ( (uint) DataStream[iBufferIndex] << 8);
                for ( int iBitIndex = 0; iBitIndex < 8; iBitIndex++ )
                {
                    if ( ( (uiCRCITTSum^uiByteValue) & 0x8000) != 0 )
                    {
                        uiCRCITTSum = (uiCRCITTSum <<1 ) ^ 0x1021;
                    }
                    else
                    {
                        uiCRCITTSum <<= 1;
                    }
                    uiByteValue <<=1;
                }
            }
            return (ushort)uiCRCITTSum;
        }
  
        // subroutines

        static ulong reflect (ulong crc, int bitnum) 
        {

	        // reflects the lower 'bitnum' bits of 'crc'

	        ulong i, j=1, crcout = 0;

	        for ( i = (ulong)1 <<(bitnum-1); i != 0; i>>=1) 
            {
                if ( ( crc & i ) != 0 ) 
                {
                    crcout |= j;
                }
		        j<<= 1;
	        }
	        return (crcout);
        }

        static void generate_crc_table() 
        {

	        // make CRC lookup table used by table algorithms

	        int i, j;
	        ulong bit, crc;

	        for (i=0; i<256; i++) 
            {
		        crc=(ulong)i;
                if ( refin !=0 ) 
                {
                        crc=reflect(crc, 8);
                }
		        crc<<= order-8;

		        for (j=0; j<8; j++) 
                {
			        bit = crc & crchighbit;
			        crc<<= 1;
			        if ( bit !=0 ) crc^= polynom;
		        }			

                if (refin != 0) 
                {
                    crc = reflect(crc, order);
                }
		        crc&= crcmask;
		        crctab[i]= crc;
	        }
        }

        static ulong crctablefast (byte[] p, int len) 
        {

	        // fast lookup table algorithm without augmented zero bytes, e.g. used in pkzip.
	        // only usable with polynom orders of 8, 16, 24 or 32.

	        ulong crc = crcinit_direct;

            if ( refin != 0 )
            {
                crc = reflect(crc, order);
            }

            if ( refin == 0 ) 
            {
                for ( int i = 0; i < p.Length; i++ )
                {
                    crc = (crc << 8) ^ crctab[ ((crc >> (order-8)) & 0xff) ^ p[i]];
                }
//                while ( len--) 
//                {
//                    crc = (crc << 8) ^ crctab[ ((crc >> (order-8)) & 0xff) ^ *p++];
//                }
            }
            else 
            {
                for ( int i = 0; i < p.Length; i++ )
                {
                    crc = (crc >> 8) ^ crctab[ (crc & 0xff) ^ p[i]];
                }
//                    while (len--) crc = (crc >> 8) ^ crctab[ (crc & 0xff) ^ *p++];
            }

            if ( (refout^refin) != 0 ) 
            {
                crc = reflect(crc, order);
            }
	        crc^= crcxor;
	        crc&= crcmask;

	        return(crc);
        }


        static ulong crctable (byte[] p, int len) 
        {
	        // normal lookup table algorithm with augmented zero bytes.
	        // only usable with polynom orders of 8, 16, 24 or 32.

	        ulong crc = crcinit_nondirect;

            if ( refin != 0 ) 
            {
                crc = reflect(crc, order);
            }

            if ( refin == 0 ) 
            {
                for ( int i = 0; i < p.Length; i++ )
                {
                    crc = ((crc << 8) | p[i]) ^ crctab[ (crc >> (order-8)) & 0xff ];
                }
                    //while (len--) crc = ((crc << 8) | *p++) ^ crctab[ (crc >> (order-8))  & 0xff];
            }
            else 
            {
                for ( int i = 0; i < p.Length; i++ )
                {
                    crc = (ulong)(( (int)(crc >> 8) | (p[i] << (order-8))) ^ (int)crctab[ crc & 0xff ]);
                }
                //while (len--) crc = ((crc >> 8) | (*p++ << (order-8))) ^ crctab[ crc & 0xff];
            }

            if ( refin == 0 ) 
            {
                for ( int i = 0; i < order/8; i++ )
                {
                    crc = (crc << 8) ^ crctab[ (crc >> (order-8))  & 0xff];
                } // while ( ++len < order/8) crc = (crc << 8) ^ crctab[ (crc >> (order-8))  & 0xff];
            }
            else 
            {
                for ( int i = 0; i < order/8; i++ )
                {
                    crc = (crc >> 8) ^ crctab[crc & 0xff];
                } //while (++len < order/8) crc = (crc >> 8) ^ crctab[crc & 0xff];
            }

            if ( (refout^refin) != 0 ) 
            {
                crc = reflect(crc, order);
            }
	        crc^= crcxor;
	        crc&= crcmask;

	        return(crc);
        }

        static ulong crcbitbybit(byte[] p, int len) 
        {
	        // bit by bit algorithm with augmented zero bytes.
	        // does not use lookup table, suited for polynom orders between 1...32.
            int i;
	        ulong  j, c, bit;
	        ulong crc = crcinit_nondirect;

	        for (i=0; i<len; i++) 
            {
		        c = (ulong)p[i];
                if ( refin != 0 ) 
                {
                    c = reflect(c, 8);
                }

		        for (j=0x80; j != 0; j>>=1) 
                {
			        bit = crc & crchighbit;
			        crc<<= 1;
                    if ( (c & j) != 0) 
                    {
                        crc|= 1;
                    }
                    if ( bit  != 0 ) 
                    {
                        crc^= polynom;
                    }
		        }
	        }	

	        for ( i=0; (int)i < order; i++) 
            {

		        bit = crc & crchighbit;
		        crc<<= 1;
		        if ( bit != 0 ) crc^= polynom;
	        }

            if ( refout != 0 ) 
            {
                crc=reflect(crc, order);
            }
	        crc^= crcxor;
	        crc&= crcmask;

	        return(crc);
        }

        static ulong crcbitbybitfast(byte[] p, int len) 
        {
	        // fast bit by bit algorithm without augmented zero bytes.
	        // does not use lookup table, suited for polynom orders between 1...32.
            int i;
	        ulong j, c, bit;
	        ulong crc = crcinit_direct;

	        for (i = 0; i < len; i++) 
            {
		        c = (ulong)p[i];
                if ( refin != 0) 
                {
                    c = reflect(c, 8);
                }

		        for ( j = 0x80; j > 0; j >>= 1 ) 
                {
			        bit = crc & crchighbit;
			        crc <<= 1;
			        if ( (c & j) > 0 ) bit^= crchighbit;
			        if ( bit > 0 ) crc^= polynom;
		        }
	        }	

            if ( refout > 0) 
            {
                crc=reflect( crc, order );
            }
	        crc^= crcxor;
	        crc&= crcmask;

	        return(crc);
        }
     }
}

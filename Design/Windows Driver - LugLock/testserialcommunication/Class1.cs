using System;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Text;
using LoMaN.IO;
using Communication.IO.Tools;


namespace TestSerialCommunication
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	/// 

    class Class1
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 

        // The main serial stream
        private static SerialStream ss;

        // Crc Computation Class
        private static CRCTool compCRC = new CRCTool();

        [STAThread]
        static void Main(string[] args)
        {
            //
            // TODO: Add code to start application here
            // Create a serial port
            ss = new SerialStream();
            try 
            {  
                // Make sure to use the right string,
                // this is directly parsed to the CreateFile
                // "\\\\.\\COM3",
                ss.Open("\\\\.\\COM3");
            }
            catch (Exception e) 
            {
                Console.WriteLine("Error: " + e.Message);
                return;
            }

            // Set port settings
            ss.SetPortSettings( 
                38400, 
                SerialStream.FlowControl.None, 
                SerialStream.Parity.None, 
                8, 
                SerialStream.StopBits.One);
            
            // Set timeout so read ends after 20ms of silence after a response
            ss.SetTimeouts(100, 0, 0, 0, 0);

            // Create the StreamWriter used to send commands
            StreamWriter sw = new StreamWriter(ss, System.Text.Encoding.ASCII);

            // Create the Thread used to read responses
            Thread responseReaderThread = new Thread(new ThreadStart(ReadResponseThread));
            responseReaderThread.Start();

            // Read all returned lines
            for (;;) 
            {
                // Read command from console
                string command = Console.ReadLine();

                // Check for exit command
                if (command.Trim().ToLower() == "exit") 
                {
                    responseReaderThread.Abort();
                    break;
                }
                
                // Write command to port
                byte[] rawBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(command);
                
                compCRC.Init(CRCTool.CRCCode.CRC_CCITT);
                ushort usCrc16 = (ushort)compCRC.crctablefast(rawBytes);

                // The following is equal to sprintf( message,"!%s|%04X\r", buf, crc );
                string sMessageString = String.Format("!{0}|{1:X4}\r",command,usCrc16);
                sw.WriteLine(sMessageString );
                sw.Flush();
            }
        }
        // Main loop for reading responses
        static void ReadResponseThread() 
        {
            StreamReader sr = new StreamReader(ss, System.Text.Encoding.ASCII);
            try 
            {
                for (;;) 
                {
                    // Read response from modem
                    string response = sr.ReadLine();
                    Console.WriteLine("Response: " + response);
                    // Check message for communication errors
                    // retrieve the Application Data frame from the code
                    // omit the leading (!) and trailing (|XXXX)
                    string appdata = response.Substring(1, (response.Length - 6) );
                    byte[] rawBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(appdata);
                    ushort usCrc16 = (ushort)compCRC.crctablefast(rawBytes);

                    // Ok check if checksum is equal
                    string sCheckString = response.Substring( (response.Length - 4), 4);
                    if ( usCrc16 != sCheckString )
                    {
                        Console.WriteLine(" Checksum error :" + sCheckString + ":" + );
                    }
                }
            }
            catch (ThreadAbortException) 
            {
            }
        }
     }
}

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/Desktop/Blocks/gr-usb_blk/build/swig/usb_blk_swigPYTHON_wrap.cxx" "/root/Desktop/Blocks/gr-usb_blk/build/swig/CMakeFiles/_usb_blk_swig.dir/usb_blk_swigPYTHON_wrap.cxx.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/root/Desktop/Blocks/gr-usb_blk/build/lib/CMakeFiles/gnuradio-usb_blk.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../lib"
  "../include"
  "lib"
  "include"
  "/usr/local/include"
  "swig"
  "../swig"
  "/usr/local/include/gnuradio/swig"
  "/usr/include/python2.7"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})

# CMake generated Testfile for 
# Source directory: /root/Desktop/Blocks/gr-usb_blk
# Build directory: /root/Desktop/Blocks/gr-usb_blk/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(include/usb_blk)
subdirs(lib)
subdirs(swig)
subdirs(python)
subdirs(grc)
subdirs(apps)
subdirs(docs)

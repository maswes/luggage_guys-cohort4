var annotated =
[
    [ "SerialPort::AlreadyOpen", "classSerialPort_1_1AlreadyOpen.html", "classSerialPort_1_1AlreadyOpen" ],
    [ "SerialPort::NotOpen", "classSerialPort_1_1NotOpen.html", "classSerialPort_1_1NotOpen" ],
    [ "SerialPort::OpenFailed", "classSerialPort_1_1OpenFailed.html", "classSerialPort_1_1OpenFailed" ],
    [ "PosixSignalHandler", "classPosixSignalHandler.html", "classPosixSignalHandler" ],
    [ "SerialPort::ReadTimeout", "classSerialPort_1_1ReadTimeout.html", "classSerialPort_1_1ReadTimeout" ],
    [ "SerialPort", "classSerialPort.html", "classSerialPort" ],
    [ "LibSerial::SerialStream", "classLibSerial_1_1SerialStream.html", "classLibSerial_1_1SerialStream" ],
    [ "LibSerial::SerialStreamBuf", "classLibSerial_1_1SerialStreamBuf.html", "classLibSerial_1_1SerialStreamBuf" ],
    [ "gr::usb_blk::tx_to_usb", "classgr_1_1usb__blk_1_1tx__to__usb.html", "classgr_1_1usb__blk_1_1tx__to__usb" ],
    [ "gr::usb_blk::tx_to_usb_impl", "classgr_1_1usb__blk_1_1tx__to__usb__impl.html", "classgr_1_1usb__blk_1_1tx__to__usb__impl" ],
    [ "SerialPort::UnsupportedBaudRate", "classSerialPort_1_1UnsupportedBaudRate.html", "classSerialPort_1_1UnsupportedBaudRate" ]
];
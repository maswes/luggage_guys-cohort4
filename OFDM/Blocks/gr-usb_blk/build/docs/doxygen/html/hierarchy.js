var hierarchy =
[
    [ "SerialPort::AlreadyOpen", "classSerialPort_1_1AlreadyOpen.html", null ],
    [ "SerialPort::NotOpen", "classSerialPort_1_1NotOpen.html", null ],
    [ "SerialPort::OpenFailed", "classSerialPort_1_1OpenFailed.html", null ],
    [ "PosixSignalHandler", "classPosixSignalHandler.html", null ],
    [ "SerialPort::ReadTimeout", "classSerialPort_1_1ReadTimeout.html", null ],
    [ "SerialPort", "classSerialPort.html", null ],
    [ "LibSerial::SerialStream", "classLibSerial_1_1SerialStream.html", null ],
    [ "LibSerial::SerialStreamBuf", "classLibSerial_1_1SerialStreamBuf.html", null ],
    [ "gr::usb_blk::tx_to_usb", "classgr_1_1usb__blk_1_1tx__to__usb.html", [
      [ "gr::usb_blk::tx_to_usb_impl", "classgr_1_1usb__blk_1_1tx__to__usb__impl.html", null ]
    ] ],
    [ "SerialPort::UnsupportedBaudRate", "classSerialPort_1_1UnsupportedBaudRate.html", null ]
];
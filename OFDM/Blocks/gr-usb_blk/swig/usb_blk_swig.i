/* -*- c++ -*- */

#define USB_BLK_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "usb_blk_swig_doc.i"

%{
#include "usb_blk/tx_to_usb.h"
%}


%include "usb_blk/tx_to_usb.h"
GR_SWIG_BLOCK_MAGIC2(usb_blk, tx_to_usb);

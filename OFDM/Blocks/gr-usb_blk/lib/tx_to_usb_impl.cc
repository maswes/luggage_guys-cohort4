/* -*- c++ -*- */
/* 
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "tx_to_usb_impl.h"

#include <iostream>
#include <cstdlib>
#include <unistd.h>


namespace gr {
  namespace usb_blk {

    tx_to_usb::sptr
    tx_to_usb::make()
    {
      return gnuradio::get_initial_sptr
        (new tx_to_usb_impl());
    }

    /*
     * The private constructor
     */
    tx_to_usb_impl::tx_to_usb_impl()
      : gr::sync_block("tx_to_usb",
              gr::io_signature::make(1, 1, 35),
              gr::io_signature::make(0, 0, 0))
    {}

    /*
     * Our virtual destructor.
     */
    tx_to_usb_impl::~tx_to_usb_impl()
    {
    }

    int
    tx_to_usb_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
    const char *in = (char *) input_items[0];

    // Do <+signal processing+>
    //
    // Open the serial port.
    //
    const char* const SERIAL_PORT_DEVICE = "/dev/ttyPS0" ;
    using namespace LibSerial ;    
    SerialStream serial_port ;
    
    serial_port.Open( SERIAL_PORT_DEVICE ) ;
    if ( ! serial_port.good() ) 
    {
        std::cerr << "Error: Could not open serial port " 
                  << SERIAL_PORT_DEVICE 
                  << std::endl ;
        exit(1) ;
    }
    //
    // Set the baud rate of the serial port.
    //
    serial_port.SetBaudRate( SerialStreamBuf::BAUD_115200 ) ;
    if ( ! serial_port.good() ) 
    {
        std::cerr << "Error: Could not set the baud rate." << std::endl ;
        exit(1) ;
    }
    //
    // Set the number of data bits.
    //
    serial_port.SetCharSize( SerialStreamBuf::CHAR_SIZE_8 ) ;
    if ( ! serial_port.good() ) 
    {
        std::cerr << "Error: Could not set the character size." << std::endl ;
        exit(1) ;
    }
    //
    // Disable parity.
    //
    serial_port.SetParity( SerialStreamBuf::PARITY_NONE ) ;
    if ( ! serial_port.good() ) 
    {
        std::cerr << "Error: Could not disable the parity." << std::endl ;
        exit(1) ;
    }
    //
    // Set the number of stop bits.
    //
    serial_port.SetNumOfStopBits( 1 ) ;
    if ( ! serial_port.good() ) 
    {
        std::cerr << "Error: Could not set the number of stop bits."
                  << std::endl ;
        exit(1) ;
    }
    //
    // Turn on hardware flow control.
    //
    serial_port.SetFlowControl( SerialStreamBuf::FLOW_CONTROL_NONE ) ;
    if ( ! serial_port.good() ) 
    {
        std::cerr << "Error: Could not use hardware flow control."
                  << std::endl ;
        exit(1) ;
    }
    //
    // Read characters from the input port and dump them to the serial 
    // port. 
    //
    //std::cerr << "Dumping file to serial port." << std::endl ;
    for (int i=0; i<noutput_items*sizeof(char[35]); i += sizeof(char[35]))
    {
      serial_port.write(in+i, 35);
    }

   // std::cerr << in << std::endl ;
   // std::cerr << "Done." << std::endl ;

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace usb_blk */
} /* namespace gr */


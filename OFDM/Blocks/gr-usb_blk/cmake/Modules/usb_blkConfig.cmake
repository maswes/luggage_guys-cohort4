INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_USB_BLK usb_blk)

FIND_PATH(
    USB_BLK_INCLUDE_DIRS
    NAMES usb_blk/api.h
    HINTS $ENV{USB_BLK_DIR}/include
        ${PC_USB_BLK_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    USB_BLK_LIBRARIES
    NAMES gnuradio-usb_blk
    HINTS $ENV{USB_BLK_DIR}/lib
        ${PC_USB_BLK_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(USB_BLK DEFAULT_MSG USB_BLK_LIBRARIES USB_BLK_INCLUDE_DIRS)
MARK_AS_ADVANCED(USB_BLK_LIBRARIES USB_BLK_INCLUDE_DIRS)


/* -*- c++ -*- */
/* 
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_USB_BLK_TX_TO_USB_H
#define INCLUDED_USB_BLK_TX_TO_USB_H

#include <usb_blk/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
  namespace usb_blk {

    /*!
     * \brief <+description of block+>
     * \ingroup usb_blk
     *
     */
    class USB_BLK_API tx_to_usb : virtual public gr::sync_block
    {
     public:
      typedef boost::shared_ptr<tx_to_usb> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of usb_blk::tx_to_usb.
       *
       * To avoid accidental use of raw pointers, usb_blk::tx_to_usb's
       * constructor is in a private implementation
       * class. usb_blk::tx_to_usb::make is the public interface for
       * creating new instances.
       */
      static sptr make();
    };

  } // namespace usb_blk
} // namespace gr

#endif /* INCLUDED_USB_BLK_TX_TO_USB_H */


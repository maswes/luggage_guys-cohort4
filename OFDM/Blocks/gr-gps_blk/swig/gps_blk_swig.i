/* -*- c++ -*- */

#define GPS_BLK_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "gps_blk_swig_doc.i"

%{
#include "gps_blk/gps_receive.h"
%}


%include "gps_blk/gps_receive.h"
GR_SWIG_BLOCK_MAGIC2(gps_blk, gps_receive);

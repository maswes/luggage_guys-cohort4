var hierarchy =
[
    [ "SerialPort::AlreadyOpen", "classSerialPort_1_1AlreadyOpen.html", null ],
    [ "PosixSignalDispatcher::CannotAttachHandler", "classPosixSignalDispatcher_1_1CannotAttachHandler.html", null ],
    [ "PosixSignalDispatcher::CannotDetachHandler", "classPosixSignalDispatcher_1_1CannotDetachHandler.html", null ],
    [ "gr::gps_blk::gps_receive", "classgr_1_1gps__blk_1_1gps__receive.html", [
      [ "gr::gps_blk::gps_receive_impl", "classgr_1_1gps__blk_1_1gps__receive__impl.html", null ]
    ] ],
    [ "SerialPort::NotOpen", "classSerialPort_1_1NotOpen.html", null ],
    [ "SerialPort::OpenFailed", "classSerialPort_1_1OpenFailed.html", null ],
    [ "PosixSignalDispatcher", "classPosixSignalDispatcher.html", null ],
    [ "PosixSignalHandler", "classPosixSignalHandler.html", null ],
    [ "SerialPort::ReadTimeout", "classSerialPort_1_1ReadTimeout.html", null ],
    [ "SerialPort", "classSerialPort.html", null ],
    [ "LibSerial::SerialStream", "classLibSerial_1_1SerialStream.html", null ],
    [ "LibSerial::SerialStreamBuf", "classLibSerial_1_1SerialStreamBuf.html", null ],
    [ "SerialPort::UnsupportedBaudRate", "classSerialPort_1_1UnsupportedBaudRate.html", null ]
];
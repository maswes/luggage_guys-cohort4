var annotated =
[
    [ "SerialPort::AlreadyOpen", "classSerialPort_1_1AlreadyOpen.html", "classSerialPort_1_1AlreadyOpen" ],
    [ "PosixSignalDispatcher::CannotAttachHandler", "classPosixSignalDispatcher_1_1CannotAttachHandler.html", "classPosixSignalDispatcher_1_1CannotAttachHandler" ],
    [ "PosixSignalDispatcher::CannotDetachHandler", "classPosixSignalDispatcher_1_1CannotDetachHandler.html", "classPosixSignalDispatcher_1_1CannotDetachHandler" ],
    [ "gr::gps_blk::gps_receive", "classgr_1_1gps__blk_1_1gps__receive.html", "classgr_1_1gps__blk_1_1gps__receive" ],
    [ "gr::gps_blk::gps_receive_impl", "classgr_1_1gps__blk_1_1gps__receive__impl.html", "classgr_1_1gps__blk_1_1gps__receive__impl" ],
    [ "SerialPort::NotOpen", "classSerialPort_1_1NotOpen.html", "classSerialPort_1_1NotOpen" ],
    [ "SerialPort::OpenFailed", "classSerialPort_1_1OpenFailed.html", "classSerialPort_1_1OpenFailed" ],
    [ "PosixSignalDispatcher", "classPosixSignalDispatcher.html", "classPosixSignalDispatcher" ],
    [ "PosixSignalHandler", "classPosixSignalHandler.html", "classPosixSignalHandler" ],
    [ "SerialPort::ReadTimeout", "classSerialPort_1_1ReadTimeout.html", "classSerialPort_1_1ReadTimeout" ],
    [ "SerialPort", "classSerialPort.html", "classSerialPort" ],
    [ "LibSerial::SerialStream", "classLibSerial_1_1SerialStream.html", "classLibSerial_1_1SerialStream" ],
    [ "LibSerial::SerialStreamBuf", "classLibSerial_1_1SerialStreamBuf.html", "classLibSerial_1_1SerialStreamBuf" ],
    [ "SerialPort::UnsupportedBaudRate", "classSerialPort_1_1UnsupportedBaudRate.html", "classSerialPort_1_1UnsupportedBaudRate" ]
];
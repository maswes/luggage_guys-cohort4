
/*
 * This file was automatically generated using swig_doc.py.
 *
 * Any changes to it will be lost next time it is regenerated.
 */




%feature("docstring") std::allocator "STL class."



%feature("docstring") SerialPort::AlreadyOpen::AlreadyOpen "

Params: (whatArg)"

%feature("docstring") std::auto_ptr "STL class."

%feature("docstring") std::auto_ptr::operator-> "STL member.

Params: (NONE)"

%feature("docstring") std::bad_alloc "STL class."

%feature("docstring") std::bad_cast "STL class."

%feature("docstring") std::bad_exception "STL class."

%feature("docstring") std::bad_typeid "STL class."

%feature("docstring") std::basic_fstream "STL class."

%feature("docstring") std::basic_ifstream "STL class."

%feature("docstring") std::basic_ios "STL class."

%feature("docstring") std::basic_iostream "STL class."

%feature("docstring") std::basic_istream "STL class."

%feature("docstring") std::basic_istringstream "STL class."

%feature("docstring") std::basic_ofstream "STL class."

%feature("docstring") std::basic_ostream "STL class."

%feature("docstring") std::basic_ostringstream "STL class."

%feature("docstring") std::basic_string "STL class."

%feature("docstring") std::basic_stringstream "STL class."

%feature("docstring") std::bitset "STL class."

%feature("docstring") PosixSignalDispatcher::CannotAttachHandler "Exception thrown when AttachHandler() fails due to a runtime error."

%feature("docstring") PosixSignalDispatcher::CannotAttachHandler::CannotAttachHandler "

Params: (whatArg)"

%feature("docstring") PosixSignalDispatcher::CannotDetachHandler "Exception thrown when DetachHandler() fails due to a runtime error."

%feature("docstring") PosixSignalDispatcher::CannotDetachHandler::CannotDetachHandler "

Params: (whatArg)"

%feature("docstring") std::complex "STL class."

%feature("docstring") std::map::const_iterator "STL iterator class."

%feature("docstring") std::basic_string::const_iterator "STL iterator class."

%feature("docstring") std::set::const_iterator "STL iterator class."

%feature("docstring") std::multiset::const_iterator "STL iterator class."

%feature("docstring") std::string::const_iterator "STL iterator class."

%feature("docstring") std::vector::const_iterator "STL iterator class."

%feature("docstring") std::wstring::const_iterator "STL iterator class."

%feature("docstring") std::multimap::const_iterator "STL iterator class."

%feature("docstring") std::deque::const_iterator "STL iterator class."

%feature("docstring") std::list::const_iterator "STL iterator class."

%feature("docstring") std::map::const_reverse_iterator "STL iterator class."

%feature("docstring") std::multimap::const_reverse_iterator "STL iterator class."

%feature("docstring") std::set::const_reverse_iterator "STL iterator class."

%feature("docstring") std::basic_string::const_reverse_iterator "STL iterator class."

%feature("docstring") std::multiset::const_reverse_iterator "STL iterator class."

%feature("docstring") std::vector::const_reverse_iterator "STL iterator class."

%feature("docstring") std::string::const_reverse_iterator "STL iterator class."

%feature("docstring") std::wstring::const_reverse_iterator "STL iterator class."

%feature("docstring") std::list::const_reverse_iterator "STL iterator class."

%feature("docstring") std::deque::const_reverse_iterator "STL iterator class."

%feature("docstring") std::deque "STL class."

%feature("docstring") std::domain_error "STL class."

%feature("docstring") std::exception "STL class."

%feature("docstring") std::ios_base::failure "STL class."

%feature("docstring") std::fstream "STL class."

%feature("docstring") gr::gps_blk::gps_receive "<+description of block+>"

%feature("docstring") gr::gps_blk::gps_receive::make "Return a shared_ptr to a new instance of gps_blk::gps_receive.

To avoid accidental use of raw pointers, gps_blk::gps_receive's constructor is in a private implementation class. gps_blk::gps_receive::make is the public interface for creating new instances.

Params: (NONE)"

%feature("docstring") std::ifstream "STL class."

%feature("docstring") std::invalid_argument "STL class."

%feature("docstring") std::ios "STL class."

%feature("docstring") std::ios_base "STL class."

%feature("docstring") std::istream "STL class."

%feature("docstring") std::istringstream "STL class."

%feature("docstring") std::deque::iterator "STL iterator class."

%feature("docstring") std::set::iterator "STL iterator class."

%feature("docstring") std::basic_string::iterator "STL iterator class."

%feature("docstring") std::multiset::iterator "STL iterator class."

%feature("docstring") std::vector::iterator "STL iterator class."

%feature("docstring") std::string::iterator "STL iterator class."

%feature("docstring") std::wstring::iterator "STL iterator class."

%feature("docstring") std::multimap::iterator "STL iterator class."

%feature("docstring") std::list::iterator "STL iterator class."

%feature("docstring") std::map::iterator "STL iterator class."

%feature("docstring") std::length_error "STL class."

%feature("docstring") std::list "STL class."

%feature("docstring") std::logic_error "STL class."

%feature("docstring") std::map "STL class."

%feature("docstring") std::multimap "STL class."

%feature("docstring") std::multiset "STL class."



%feature("docstring") SerialPort::NotOpen::NotOpen "

Params: (whatArg)"

%feature("docstring") std::ofstream "STL class."



%feature("docstring") SerialPort::OpenFailed::OpenFailed "

Params: (whatArg)"

%feature("docstring") std::ostream "STL class."

%feature("docstring") std::ostringstream "STL class."

%feature("docstring") std::out_of_range "STL class."

%feature("docstring") std::overflow_error "STL class."



%feature("docstring") PosixSignalDispatcher::Instance "This is a singleton class and there is only one instance of this class per process. This instance can be obtained using the GetInstance() method.

Params: (NONE)"

%feature("docstring") PosixSignalDispatcher::AttachHandler "Attach a signal handler to the signal dispatcher. The signal handler's HandlePosixSignal() method will be called every time the specified signal is received. The signal handler should not be destroyed while it attached to the signal dispatcher. Otherwise, weird things are bound to happen (i.e. \"undefined
 behavior\" shall ensue). Make sure you call DetachHandler() from the destructor of the signal handler.

If a PosixSignalHandler is attached to the same signal number multiple times, it will be called multiple times. Furthermore, it should also be detached as many times as it was attached before it is destroyed. Otherwise, undefined behavior may result.

Params: (posixSignalNumber, signalHandler)"

%feature("docstring") PosixSignalDispatcher::DetachHandler "Detach the specified signal handler from the signal dispatcher. The signal handler will stop being called on receiving the corresponding POSIX signal. If the signal handler is not attached to the signal dispatcher when this method is called then this method has no effect.

Params: (posixSignalNumber, signalHandler)"

%feature("docstring") PosixSignalDispatcher::PosixSignalDispatcher "This is a singleton class and the only instances of this class can only be accessed using the Instance() method. This is enforced by making the default constructor a private member disalloweing construction of new instances of this class

Params: (NONE)"

%feature("docstring") PosixSignalDispatcher::~PosixSignalDispatcher "This class cannot be subclassed. We enforce this by making the destructor a private member.

Params: (NONE)"

%feature("docstring") PosixSignalDispatcher::operator= "Copying of an instance of this class is not allowed. We enforce this by making the copy constructor and the assignment operator private members.

Params: (otherInstance)"

%feature("docstring") PosixSignalHandler "Gets a method called when the corresponding signal is received. A PosixSignalHandler must be connected to the PosixSignalDispatcher for it to be called."

%feature("docstring") PosixSignalHandler::HandlePosixSignal "This method is called when the specified POSIX signal is received by the PosixSignalDispatcher that is managing this handler.

Params: (signalNumber)"

%feature("docstring") PosixSignalHandler::~PosixSignalHandler "Destructor is declared virtual as we expect this class to be subclassed. It is also declared pure abstract to make this class a pure abstract class.

Params: (NONE)"

%feature("docstring") std::priority_queue "STL class."

%feature("docstring") std::queue "STL class."

%feature("docstring") std::range_error "STL class."



%feature("docstring") SerialPort::ReadTimeout::ReadTimeout "

Params: (NONE)"

%feature("docstring") std::list::reverse_iterator "STL iterator class."

%feature("docstring") std::multimap::reverse_iterator "STL iterator class."

%feature("docstring") std::string::reverse_iterator "STL iterator class."

%feature("docstring") std::multiset::reverse_iterator "STL iterator class."

%feature("docstring") std::map::reverse_iterator "STL iterator class."

%feature("docstring") std::vector::reverse_iterator "STL iterator class."

%feature("docstring") std::set::reverse_iterator "STL iterator class."

%feature("docstring") std::wstring::reverse_iterator "STL iterator class."

%feature("docstring") std::basic_string::reverse_iterator "STL iterator class."

%feature("docstring") std::deque::reverse_iterator "STL iterator class."

%feature("docstring") std::runtime_error "STL class."

%feature("docstring") SerialPort ":FIXME: Provide examples of the above potential problem."

%feature("docstring") SerialPort::SerialPort "Constructor for a serial port.

Params: (serialPortName)"

%feature("docstring") SerialPort::~SerialPort "Destructor.

Params: (NONE)"

%feature("docstring") SerialPort::Open "Open the serial port with the specified settings. A serial port cannot be used till it is open.

Params: (baudRate, charSize, parityType, stopBits, flowControl)"

%feature("docstring") SerialPort::IsOpen "Check if the serial port is open for I/O.

Params: (NONE)"

%feature("docstring") SerialPort::Close "Close the serial port. All settings of the serial port will be lost and no more I/O can be performed on the serial port.

Params: (NONE)"

%feature("docstring") SerialPort::SetBaudRate "Set the baud rate for the serial port to the specified value (baudRate).

Params: (baudRate)"

%feature("docstring") SerialPort::GetBaudRate "Get the current baud rate for the serial port.

Params: (NONE)"

%feature("docstring") SerialPort::SetCharSize "Set the character size for the serial port.

Params: (charSize)"

%feature("docstring") SerialPort::GetCharSize "Get the current character size for the serial port.

Params: (NONE)"

%feature("docstring") SerialPort::SetParity "Set the parity type for the serial port.

Params: (parityType)"

%feature("docstring") SerialPort::GetParity "Get the parity type for the serial port.

Params: (NONE)"

%feature("docstring") SerialPort::SetNumOfStopBits "Set the number of stop bits to be used with the serial port.

Params: (numOfStopBits)"

%feature("docstring") SerialPort::GetNumOfStopBits "Get the number of stop bits currently being used by the serial port.

Params: (NONE)"

%feature("docstring") SerialPort::SetFlowControl "Set flow control.

Params: (flowControl)"

%feature("docstring") SerialPort::GetFlowControl "Get the current flow control setting.

Params: (NONE)"

%feature("docstring") SerialPort::IsDataAvailable "Check if data is available at the input of the serial port.

Params: (NONE)"

%feature("docstring") SerialPort::ReadByte "Read a single byte from the serial port. If no data is available in the specified number of milliseconds (msTimeout), then this method will throw ReadTimeout exception. If msTimeout is 0, then this method will block till data is available.

Params: (msTimeout)"

%feature("docstring") SerialPort::Read "

Params: (dataBuffer, numOfBytes, msTimeout)"

%feature("docstring") SerialPort::ReadLine "Read a line of characters from the serial port.

Params: (msTimeout, lineTerminator)"

%feature("docstring") SerialPort::WriteByte "Send a single byte to the serial port.

Params: (dataByte)"

%feature("docstring") SerialPort::Write "Write the data from the specified vector to the serial port.

Params: (dataBuffer)"

%feature("docstring") SerialPort::SetDtr "Set the DTR line to the specified value.

Params: (dtrState)"

%feature("docstring") SerialPort::GetDtr "Get the status of the DTR line.

Params: (NONE)"

%feature("docstring") SerialPort::SetRts "Set the RTS line to the specified value.

Params: (rtsState)"

%feature("docstring") SerialPort::GetRts "Get the status of the RTS line.

Params: (NONE)"

%feature("docstring") SerialPort::GetCts "

Params: (NONE)"

%feature("docstring") SerialPort::GetDsr "

Params: (NONE)"

%feature("docstring") SerialPort::operator= "Prevent copying of objects of this class by declaring the assignment operator private. This method is never defined.

Params: (otherSerialPort)"

%feature("docstring") LibSerial::SerialStream "A stream class for accessing serial ports on POSIX operating systems. A lot of the functionality of this class has been obtained by looking at the code of libserial package by Linas Vepstas () and the excellent document on serial programming by Michael R. Sweet. This document can be found at . The libserial package can be found at . This class allows one to set various parameters of a serial port and then access it like a simple fstream. In fact, that is exactly what it does. It sets the parameters of the serial port by maintaining a file descriptor for the port and uses the basic_fstream functions for the IO. We have not implemented any file locking yet but it will be added soon.

Make sure you read the documentation of the standard fstream template before using this class because most of the functionality is inherited from fstream. Also a lot of information about the various system calls used in the implementation can also be found in the Single Unix Specification (Version 2). A copy of this document can be obtained from . We will refer to this document as SUS-2."

%feature("docstring") LibSerial::SerialStream::SerialStream "This constructor takes a filename and an openmode to construct a SerialStream object. This results in a call to basic_fstream::open(s,mode). This is the only way to contruct an object of this class. We have to enforce this instead of providing a default constructor because we want to get a file descriptor whenever the basic_fstream::open() function is called. However, this function is not made virtual in the STL hence it is probably not very safe to overload it. We may decide to overload it later but the users of this class will have to make sure that this class is not used as an fstream class. The SerialStream will be in the \"open\" state (same state as after calling the Open() method) after calling this constructor.

If the constructor has problems opening the serial port or getting the file-descriptor for the port, it will set the failbit for the stream. So, one must make sure that the stream is in a good state before using it for any further I/O operations.

Params: (fileName, openMode)"

%feature("docstring") LibSerial::SerialStream::~SerialStream "The destructor. It closes the stream associated with mFileDescriptor. The rest is done by the fstream destructor.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStream::Open "Open the serial port associated with the specified filename, and the specified mode, mode.

Params: (fileName, openMode)"

%feature("docstring") LibSerial::SerialStream::Close "Close the serial port. No communications can occur with the serial port after calling this routine.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStream::IsOpen "Returns true if the Stream is in a good open state, false otherwise

Params: (NONE)"

%feature("docstring") LibSerial::SerialStream::SetBaudRate "Set the baud rate for serial communications.

Params: (baudRate)"

%feature("docstring") LibSerial::SerialStream::BaudRate "Get the current baud rate being used for serial communication. This routine queries the serial port for its current settings and returns the baud rate that is being used by the serial port.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStream::SetCharSize "Set the character size associated with the serial port.

Params: (charSize)"

%feature("docstring") LibSerial::SerialStream::CharSize "Get the character size being used for serial communication.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStream::SetNumOfStopBits "Set the number of stop bits used during serial communication. The only valid values are 1 and 2.

Params: (numOfStopBits)"

%feature("docstring") LibSerial::SerialStream::NumOfStopBits "Get the number of stop bits being used during serial communication.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStream::SetParity "Set the parity for serial communication.

Params: (parityType)"

%feature("docstring") LibSerial::SerialStream::Parity "Get the current parity setting for the serial port.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStream::SetFlowControl "Use the specified flow control.

Params: (flowControlType)"

%feature("docstring") LibSerial::SerialStream::FlowControl "Return the current flow control setting.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStream::SetVMin "Set character buffer size.

Params: (vtime)"

%feature("docstring") LibSerial::SerialStream::VMin "Get current size of character buffer. Look  for more documentation about VTIME and VMIN.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStream::SetVTime "Set character buffer timing in 10th of a second.

Params: (vtime)"

%feature("docstring") LibSerial::SerialStream::VTime "Get current timing of character buffer in 10th of a second. Look  for more documentation about VTIME and VMIN.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStream::operator= "

Params: ()"

%feature("docstring") LibSerial::SerialStreamBuf "This is the streambuf subclass used by SerialStream. This subclass takes care of opening the serial port file in the required modes and providing the corresponding file descriptor to SerialStream so that various parameters associated with the serial port can be set. Several features of this streambuf class resemble those of std::filebuf, however this class it not made a subclass of filebuf because we need access to the file descriptor associated with the serial port and the standard filebuf does not provide access to it.

At present, this class uses unbuffered I/O and all calls to setbuf() will be ignored."

%feature("docstring") LibSerial::SerialStreamBuf::SerialStreamBuf "The default constructor.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStreamBuf::~SerialStreamBuf "The destructor.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStreamBuf::is_open "Returns true if a previous call to open() succeeded (returned a non-null value) and there has been no intervening call to close.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStreamBuf::open "If is_open() != , returns a null pointer. Otherwise, initializes the  as required. It then opens a file, if possible, whose name is given as the string  using the system call . The value of parameter  is obtained from the value of the parameter mode. At present, only , , and () make sense for a serial port and hence all other settings result in the call to fail. The value of  is obtained as: 


 


where  is obtained from the following table depending on the value of the parameter mode:

Params: (filename, mode)"

%feature("docstring") LibSerial::SerialStreamBuf::close "If is_open() == false, returns a null pointer. If a put area exists, calls overflow(EOF) to flush characters. Finally it closes the file by calling  where mFileDescriptor is the value returned by the last call to open().

For the implementation of the corresponding function in class filebuf, if the last virtual member function called on  (between underflow, overflow, , and ) was overflow then it calls  (possible several times) to determine a termination sequence, inserts those characters and calls overflow(EOF) again. However, .

: is_open() ==

Params: (NONE)"

%feature("docstring") LibSerial::SerialStreamBuf::SetParametersToDefault "Initialize the serial communication parameters to their default values.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStreamBuf::SetBaudRate "If is_open() != true, return -1. Otherwise, set the baud rate of the associated serial port. Return the baud rate on success and BAUD_INVALID on failure.

Params: (baudRate)"

%feature("docstring") LibSerial::SerialStreamBuf::BaudRate "Return the current baud rate of the serial port. If the baud rate is not set to a valid value then it returns BAUD_INVALID.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStreamBuf::SetCharSize "Set the character size to be used during serial communication. It returns the character size on success and CHAR_SIZE_INVALID on failure.

Params: (charSize)"

%feature("docstring") LibSerial::SerialStreamBuf::CharSize "Return the character size currently being used for serial communication.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStreamBuf::SetNumOfStopBits "Set the number of stop bits used during serial communication. The only valid values are 1 and 2.

Params: (numOfStopBits)"

%feature("docstring") LibSerial::SerialStreamBuf::NumOfStopBits "Get the number of stop bits being used during serial communication.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStreamBuf::SetParity "Set the parity for serial communication.

Params: (parityType)"

%feature("docstring") LibSerial::SerialStreamBuf::Parity "Get the current parity setting for the serial port.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStreamBuf::SetFlowControl "Use the specified flow control.

Params: (flowControlType)"

%feature("docstring") LibSerial::SerialStreamBuf::FlowControl "Return the current flow control setting.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStreamBuf::SetVMin "Set the minimum number of characters for non-canonical reads. See VMIN in man termios(3).

Params: (vtime)"

%feature("docstring") LibSerial::SerialStreamBuf::VMin "Get the VMIN value for the device. This represents the minimum number of characters for non-canonical reads.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStreamBuf::SetVTime "Set character buffer timeout in 10ths of a second. This applies to non-canonical reads.

Params: (vtime)"

%feature("docstring") LibSerial::SerialStreamBuf::VTime "Get the current timeout value for non-canonical reads in deciseconds.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStreamBuf::setbuf "Performs an operation that is defined separately for each class derived from streambuf. The default behavior is to do nothing if gptr() is non-null and gptr()!=egptr(). Also, setbuf(0, 0) usually means unbuffered I/O and setbuf(p, n) means use p[0]...p[n-1] to hold the buffered characters. In general, this method implements the subclass's notion of getting memory for the buffered characters.

In the case of SerialStreamBuf, we want to keep using unbuffered I/O. Hence, using this method has no effect at present.

Params: (, )"

%feature("docstring") LibSerial::SerialStreamBuf::xsgetn "Reads upto n characters from the serial port and returns them through the character array located at s.

Params: (s, n)"

%feature("docstring") LibSerial::SerialStreamBuf::showmanyc "Check, wether input is available on the port. If you call , this method will be called to check for available input.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStreamBuf::underflow "Reads and returns the next character from the associated serial port if one otherwise returns traits::eof(). This method is used for buffered I/O while uflow() is called for unbuffered I/O.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStreamBuf::uflow "Reads and returns the next character from the associated serial port if one otherwise returns traits::eof(). This method is used for unbuffered I/O while underflow() is called for unbuffered I/O.

Params: (NONE)"

%feature("docstring") LibSerial::SerialStreamBuf::pbackfail "This function is called when a putback of a character fails. This must be implemented for unbuffered I/O as all streambuf subclasses are required to provide putback of at least on character.

Params: (c)"

%feature("docstring") LibSerial::SerialStreamBuf::xsputn "Writes upto n characters from the character sequence at s to the serial port associated with the buffer.

Params: (s, n)"

%feature("docstring") LibSerial::SerialStreamBuf::overflow "Writes the specified character to the associated serial port.

Params: (c)"

%feature("docstring") LibSerial::SerialStreamBuf::operator= "

Params: ()"

%feature("docstring") std::set "STL class."

%feature("docstring") std::smart_ptr "STL class."

%feature("docstring") std::smart_ptr::operator-> "STL member.

Params: (NONE)"

%feature("docstring") std::stack "STL class."

%feature("docstring") std::string "STL class."

%feature("docstring") std::stringstream "STL class."

%feature("docstring") std::underflow_error "STL class."

%feature("docstring") std::unique_ptr "STL class."

%feature("docstring") std::unique_ptr::operator-> "STL member.

Params: (NONE)"



%feature("docstring") SerialPort::UnsupportedBaudRate::UnsupportedBaudRate "

Params: (whatArg)"

%feature("docstring") std::valarray "STL class."

%feature("docstring") std::vector "STL class."

%feature("docstring") std::weak_ptr "STL class."

%feature("docstring") std::weak_ptr::operator-> "STL member.

Params: (NONE)"

%feature("docstring") std::wfstream "STL class."

%feature("docstring") std::wifstream "STL class."

%feature("docstring") std::wios "STL class."

%feature("docstring") std::wistream "STL class."

%feature("docstring") std::wistringstream "STL class."

%feature("docstring") std::wofstream "STL class."

%feature("docstring") std::wostream "STL class."

%feature("docstring") std::wostringstream "STL class."

%feature("docstring") std::wstring "STL class."

%feature("docstring") std::wstringstream "STL class."
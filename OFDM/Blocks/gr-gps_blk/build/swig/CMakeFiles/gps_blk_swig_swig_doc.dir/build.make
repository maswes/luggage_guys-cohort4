# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.2

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/local/bin/cmake

# The command to remove a file.
RM = /usr/local/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /root/Desktop/Blocks/gr-gps_blk

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /root/Desktop/Blocks/gr-gps_blk/build

# Utility rule file for gps_blk_swig_swig_doc.

# Include the progress variables for this target.
include swig/CMakeFiles/gps_blk_swig_swig_doc.dir/progress.make

swig/CMakeFiles/gps_blk_swig_swig_doc: swig/gps_blk_swig_doc.i

swig/gps_blk_swig_doc.i: swig/gps_blk_swig_doc_swig_docs/xml/index.xml
	$(CMAKE_COMMAND) -E cmake_progress_report /root/Desktop/Blocks/gr-gps_blk/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold "Generating python docstrings for gps_blk_swig_doc"
	cd /root/Desktop/Blocks/gr-gps_blk/docs/doxygen && /usr/bin/python2 -B /root/Desktop/Blocks/gr-gps_blk/docs/doxygen/swig_doc.py /root/Desktop/Blocks/gr-gps_blk/build/swig/gps_blk_swig_doc_swig_docs/xml /root/Desktop/Blocks/gr-gps_blk/build/swig/gps_blk_swig_doc.i

swig/gps_blk_swig_doc_swig_docs/xml/index.xml: swig/_gps_blk_swig_doc_tag
	$(CMAKE_COMMAND) -E cmake_progress_report /root/Desktop/Blocks/gr-gps_blk/build/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold "Generating doxygen xml for gps_blk_swig_doc docs"
	cd /root/Desktop/Blocks/gr-gps_blk/build/swig && ./_gps_blk_swig_doc_tag
	cd /root/Desktop/Blocks/gr-gps_blk/build/swig && /usr/bin/doxygen /root/Desktop/Blocks/gr-gps_blk/build/swig/gps_blk_swig_doc_swig_docs/Doxyfile

gps_blk_swig_swig_doc: swig/CMakeFiles/gps_blk_swig_swig_doc
gps_blk_swig_swig_doc: swig/gps_blk_swig_doc.i
gps_blk_swig_swig_doc: swig/gps_blk_swig_doc_swig_docs/xml/index.xml
gps_blk_swig_swig_doc: swig/CMakeFiles/gps_blk_swig_swig_doc.dir/build.make
.PHONY : gps_blk_swig_swig_doc

# Rule to build all files generated by this target.
swig/CMakeFiles/gps_blk_swig_swig_doc.dir/build: gps_blk_swig_swig_doc
.PHONY : swig/CMakeFiles/gps_blk_swig_swig_doc.dir/build

swig/CMakeFiles/gps_blk_swig_swig_doc.dir/clean:
	cd /root/Desktop/Blocks/gr-gps_blk/build/swig && $(CMAKE_COMMAND) -P CMakeFiles/gps_blk_swig_swig_doc.dir/cmake_clean.cmake
.PHONY : swig/CMakeFiles/gps_blk_swig_swig_doc.dir/clean

swig/CMakeFiles/gps_blk_swig_swig_doc.dir/depend:
	cd /root/Desktop/Blocks/gr-gps_blk/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /root/Desktop/Blocks/gr-gps_blk /root/Desktop/Blocks/gr-gps_blk/swig /root/Desktop/Blocks/gr-gps_blk/build /root/Desktop/Blocks/gr-gps_blk/build/swig /root/Desktop/Blocks/gr-gps_blk/build/swig/CMakeFiles/gps_blk_swig_swig_doc.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : swig/CMakeFiles/gps_blk_swig_swig_doc.dir/depend


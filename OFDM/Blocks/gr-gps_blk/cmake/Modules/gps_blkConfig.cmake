INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_GPS_BLK gps_blk)

FIND_PATH(
    GPS_BLK_INCLUDE_DIRS
    NAMES gps_blk/api.h
    HINTS $ENV{GPS_BLK_DIR}/include
        ${PC_GPS_BLK_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    GPS_BLK_LIBRARIES
    NAMES gnuradio-gps_blk
    HINTS $ENV{GPS_BLK_DIR}/lib
        ${PC_GPS_BLK_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GPS_BLK DEFAULT_MSG GPS_BLK_LIBRARIES GPS_BLK_INCLUDE_DIRS)
MARK_AS_ADVANCED(GPS_BLK_LIBRARIES GPS_BLK_INCLUDE_DIRS)


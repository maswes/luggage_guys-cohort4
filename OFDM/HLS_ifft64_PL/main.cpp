#include <stdint.h>
#include "xilly_debug.h"
#include "fft.h"

void xillybus_wrapper(DTYPE *in, DTYPE *out) {
#pragma AP interface ap_fifo port=in
#pragma AP interface ap_fifo port=out
#pragma AP interface ap_ctrl_none port=return

	//DTYPE SIG_IN_R[SIZE], SIG_IN_I[SIZE], SIG_OUT_R[SIZE], SIG_OUT_I[SIZE],
	DTYPE X_R[SIZE], X_I[SIZE];
	DTYPE Out_R[SIZE], Out_I[SIZE];
	DTYPE buf_r, buf_i;

	DTYPE inr, ini, outr, outi;

	for (int i=0; i<SIZE; i++)
	{

	  inr = *in++;
	  ini = *in++;

	  X_R[i] = *((DTYPE*)&inr);
	  X_I[i] = *((DTYPE*)&ini);
	}

	fft(X_I, X_R, Out_R, Out_I);

	for(int i=0; i<SIZE; i++)
	{
		if (i%2==0)
		{
			outi = Out_I[i];
			outr = Out_R[i];
			*out++ = *((DTYPE*)&outi);
			*out++ = *((DTYPE*)&outr);
		}
		else
		{
			outi = -1*Out_I[i];
			outr = -1*Out_R[i];
			*out++ = *((DTYPE*)&outi);
			*out++ = *((DTYPE*)&outr);
		}
	}
}

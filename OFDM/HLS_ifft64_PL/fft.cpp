/*
This is traditional 2-radix DIT FFT algorithm implementation.
INPUT:
	In_R, In_I[]: Real and Imag parts of Complex signal

OUTPUT:
	Out_R, Out_I[]: Real and Imag parts of Complex signal
*/

#include <stdio.h>
#include "fft.h"

void fft_stage_first(DTYPE X_R[SIZE], DTYPE X_I[SIZE], DTYPE OUT_R[SIZE], DTYPE OUT_I[SIZE]);
void fft_stages(DTYPE X_R[SIZE], DTYPE X_I[SIZE], int STAGES, DTYPE OUT_R[SIZE], DTYPE OUT_I[SIZE]);

void fft(DTYPE IN_R[SIZE], DTYPE IN_I[SIZE], DTYPE OUT_R[SIZE], DTYPE OUT_I[SIZE])
{
#pragma HLS DATAFLOW

	//Call fft
	DTYPE Stage1_R[SIZE], Stage1_I[SIZE];
#pragma HLS ARRAY_PARTITION variable=Stage1_R cyclic factor=2 dim=1
	DTYPE Stage2_R[SIZE], Stage2_I[SIZE];
#pragma HLS ARRAY_PARTITION variable=Stage2_R cyclic factor=2 dim=1
	DTYPE Stage3_R[SIZE], Stage3_I[SIZE];
//#pragma HLS ARRAY_PARTITION variable=Stage3_R cyclic factor=2 dim=1
	DTYPE Stage4_R[SIZE], Stage4_I[SIZE];
//#pragma HLS ARRAY_PARTITION variable=Stage4_R cyclic factor=2 dim=1
	DTYPE Stage5_R[SIZE], Stage5_I[SIZE];
//#pragma HLS ARRAY_PARTITION variable=Stage5_R cyclic factor=2 dim=1
	//DTYPE Stage6_R[SIZE], Stage6_I[SIZE];
//#pragma HLS ARRAY_PARTITION variable=Stage6_R cyclic factor=2 dim=1
	//DTYPE Stage7_R[SIZE], Stage7_I[SIZE];
//#pragma HLS ARRAY_PARTITION variable=Stage7_R cyclic factor=2 dim=1
	//DTYPE Stage8_R[SIZE], Stage8_I[SIZE];
//#pragma HLS ARRAY_PARTITION variable=Stage8_R cyclic factor=2 dim=1
	//DTYPE Stage9_R[SIZE], Stage9_I[SIZE];
//#pragma HLS ARRAY_PARTITION variable=Stage9_R cyclic factor=2 dim=1

	fft_stage_first(IN_R, IN_I, Stage1_R, Stage1_I);
	fft_stages(Stage1_R, Stage1_I, 2, Stage2_R, Stage2_I);
	fft_stages(Stage2_R, Stage2_I, 3, Stage3_R, Stage3_I);
	fft_stages(Stage3_R, Stage3_I, 4, Stage4_R, Stage4_I);
	fft_stages(Stage4_R, Stage4_I, 5, Stage5_R, Stage5_I);
	fft_stages(Stage5_R, Stage5_I, 6, OUT_R, OUT_I);
	//fft_stages(Stage6_R, Stage6_I, 7, Stage7_R, Stage7_I);
	//fft_stages(Stage7_R, Stage7_I, 8, Stage8_R, Stage8_I);
	//fft_stages(Stage8_R, Stage8_I, 9, Stage9_R, Stage9_I);
	//fft_stages(Stage9_R, Stage9_I, 10, OUT_R, OUT_I);

}

/*=======================BEGIN: FFT=========================*/
//stage 1
void fft_stage_first(DTYPE IN_R[SIZE], DTYPE IN_I[SIZE], DTYPE OUT_R[SIZE], DTYPE OUT_I[SIZE]) {
//#pragma HLS DATAFLOW

	DTYPE X_R[SIZE], X_I[SIZE];

	#pragma HLS ARRAY_PARTITION variable=X_R cyclic factor=2 dim=1
	#pragma HLS ARRAY_PARTITION variable=X_I cyclic factor=2 dim=1
	#pragma HLS ARRAY_PARTITION variable=OUT_I cyclic factor=2 dim=1
	#pragma HLS ARRAY_PARTITION variable=IN_I cyclic factor=2 dim=1
	#pragma HLS ARRAY_PARTITION variable=OUT_R cyclic factor=2 dim=1
	#pragma HLS ARRAY_PARTITION variable=IN_R cyclic factor=2 dim=1

	for(unsigned int i=0; i<SIZE/2; i++){
	#pragma HLS UNROLL
		X_R[bit_reverse_table[i]] = IN_R[i];
		X_I[bit_reverse_table[i]] = IN_I[i];
	}

	for(unsigned int i=SIZE/2; i<SIZE; i++){
	#pragma HLS UNROLL
		X_R[bit_reverse_table[i]] = IN_R[i];
		X_I[bit_reverse_table[i]] = IN_I[i];
	}

	//Write code that computes first stage of FFT
	for (int i=0; i<SIZE; i=i+2){
	//#pragma HLS PIPELINE
	//#pragma HLS LOOP_TRIPCOUNT
		OUT_R[i] = X_R[i] + X_R[i+1];
		OUT_R[i+1] = X_R[i] - X_R[i+1];

		OUT_I[i] = X_I[i] + X_I[i+1];
		OUT_I[i+1] = X_I[i] - X_I[i+1];
	}

/*
	unsigned int x, y;
	//Insert your code here
	for (int i=0; i<SIZE; i=i+2){
	#pragma HLS UNROLL
		x=i;
		x = (((x & 0xaaaaaaaa) >> 1) | ((x & 0x55555555) << 1));
		x = (((x & 0xcccccccc) >> 2) | ((x & 0x33333333) << 2));
		x = (((x & 0xf0f0f0f0) >> 4) | ((x & 0x0f0f0f0f) << 4));
		x = (((x & 0xff00ff00) >> 8) | ((x & 0x00ff00ff) << 8));
		x = ((x >> 16) | (x << 16));
		x = x >> 22;

		y=i+1;
		y = (((y & 0xaaaaaaaa) >> 1) | ((y & 0x55555555) << 1));
		y = (((y & 0xcccccccc) >> 2) | ((y & 0x33333333) << 2));
		y = (((y & 0xf0f0f0f0) >> 4) | ((y & 0x0f0f0f0f) << 4));
		y = (((y & 0xff00ff00) >> 8) | ((y & 0x00ff00ff) << 8));
		y = ((y >> 16) | (y << 16));
		y = y >> 22;

		OUT_R[i] = X_R[x] + X_R[y];
		OUT_R[i+1] = X_R[x] - X_R[y];

		OUT_I[i] = X_I[x] + X_I[y];
		OUT_I[i+1] = X_I[x] - X_I[y];
	}
	*/
}

//stages
void fft_stages(DTYPE X_R[SIZE], DTYPE X_I[SIZE], int stage, DTYPE OUT_R[SIZE], DTYPE OUT_I[SIZE]) {

	int coef_num = (1 << stage)/2 ;
	int index = 0;
	DTYPE temp_R, temp_I;
	int i_lower = 0;
	int step = (SIZE/2/coef_num);
	//Write a code that computes any arbitary stages of the FFT
	for (int i=0; i<SIZE; i++){
		//#pragma HLS PIPELINE II=1
		index = ((i % (SIZE/2)) % coef_num) * step;

		i_lower = i + coef_num;

		temp_R = X_R[i_lower]*W_real[index]- X_I[i_lower]*W_imag[index];
		temp_I = X_I[i_lower]*W_real[index]+ X_R[i_lower]*W_imag[index];

		OUT_R[i] = X_R[i] + temp_R;
		OUT_I[i] = X_I[i] + temp_I;

		OUT_R[i_lower] = X_R[i] - temp_R;
		OUT_I[i_lower] = X_I[i] - temp_I;

		if (i%coef_num + 1 == coef_num){
			i = i + coef_num;
		}
	}
}
/*=======================END: FFT=========================*/





#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "modulator.h"

#define COUNT  66048

int main()
{
	float inVec[COUNT] = {0};
	float outVec[COUNT*2] = {0};
	float phase;

	FILE *fp_r;
	char string[64];
	char angle[32], Q_val[32];
	fp_r = fopen("input_ascii.txt", "r");
	if (fp_r == NULL)
	{
		printf("cannot open i.txt");
		return 1;
	}

	printf("inVec[0] %f\n",inVec[0]);
	printf("inVec[1] %f\n",inVec[1]);
	int k, indx;
	for (k=0, indx=0; k<COUNT; k++, indx=indx+2)
	{
		fscanf(fp_r, "%s", string);
		sscanf(string,"%[^,],%[^,]", angle);
		phase = atof(angle);
		xillybus_wrapper((int*)&phase, (int*)&outVec[indx]);
	}
	fclose(fp_r);

	fp_r = fopen("output_ascii.txt", "r");
	if (fp_r == NULL)
	{
		printf("cannot open golden.txt");
		return 1;
	}
	float output;
	int fail = 0;

//	for (k=0, indx=0; k<COUNT; k++, indx=indx+2)
//	{
//		fscanf(fp_r, "%s", string);
//		sscanf(string,"%[^,],%[^,]", I_val, Q_val);
//
//		if ( fabs(outVec[indx]-atof(I_val)) > 0.000001 )
//		{
////			printf("%d, R_output=%.10f\tR_golden=%.10f,\t\t",k, outVec[indx], atof(I_val));
//			fail = 1;
//		}
//		if ( fabs(outVec[indx+1]-atof(Q_val)) > 0.000001 )
//		{
////			printf("%d, I_output=%.10f\tI_golden=%.10f\n",k, outVec[indx+1], atof(Q_val));
//			fail = 1;
//		}
//	}

	fclose(fp_r);

	if (fail)
		printf("Failed!!\n");
	else
		printf("Success!!\n");

	return 0;
}

#include <stdio.h>
#include <math.h>
#include <errno.h>
#include "modulator.h"
static float d_phase = 0;
void sincos(INTTYPE x, float *s, float *c);
INTTYPE float_to_fixed(float x);
float floatMod(float a, float b);
void modulate(float *oq, float *oi);

/*
 * INTTYPE float_to_fixed(float x)
 */
INTTYPE float_to_fixed(float x)
{
  // Fold x into -PI to PI.
  int d = (int)floor(x/2/PI+0.5);
  x -= d*2*PI;
  // And convert to an integer.
  return (INTTYPE) ((float) x * TWO_TO_THE_31 / PI);
}

/*
 * void sincos(INTTYPE x, float s, float c)
 */
void sincos(INTTYPE x, float *s, float *c)
{
  unsigned int ux = x;
  int sin_index = ux >> (WORDBITS - NBITS);
  *s = s_sine_table[sin_index][0] * (ux >> 1) + s_sine_table[sin_index][1];

  ux = x + 0x40000000;
  int cos_index = ux >> (WORDBITS - NBITS);
  *c = s_sine_table[cos_index][0] * (ux >> 1) + s_sine_table[cos_index][1];
}

/*
 * float floatMod(float a, float b)
 */
float floatMod(float a, float b)
{
    return (a - b * floor(a / b));
}

/*
 * float modulate()
 */
void modulate(float *oq, float *oi)
{
  d_phase = floatMod(d_phase + (float)PI, 2.0f * (float)PI) - (float)PI;
  INTTYPE angle = float_to_fixed (d_phase);
  sincos(angle, oq, oi);
}


void xillybus_wrapper(int *in, int *out) {
#pragma AP interface ap_fifo port=in
#pragma AP interface ap_fifo port=out
#pragma AP interface ap_ctrl_none port=return
//  static int counter = 0;

  INTTYPE input_tmp, output_i, output_q = 0;
  DTYPE oq, oi = 0.0;
  input_tmp = *in++;
  float input = *(float *)&input_tmp;
  d_phase = d_phase + d_sensitivity * input;
  modulate(&oq, &oi);

  output_q = *((int *)&oq);
  output_i = *((int *)&oi);
  *out++ =  output_i;
  *out++ =output_q;
}

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/Desktop/gr-xillybus/gr-xillybus/swig/xillybus_swigPYTHON_wrap.cxx" "/root/Desktop/gr-xillybus/gr-xillybus/swig/CMakeFiles/_xillybus_swig.dir/xillybus_swigPYTHON_wrap.cxx.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/root/Desktop/gr-xillybus/gr-xillybus/lib/CMakeFiles/gnuradio-xillybus.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "lib"
  "include"
  "/usr/local/include"
  "swig"
  "/usr/local/include/gnuradio/swig"
  "/usr/include/python2.7"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})

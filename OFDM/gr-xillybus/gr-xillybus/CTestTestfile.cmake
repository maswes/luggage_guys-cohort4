# CMake generated Testfile for 
# Source directory: /root/Desktop/gr-xillybus/gr-xillybus
# Build directory: /root/Desktop/gr-xillybus/gr-xillybus
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(include/xillybus)
subdirs(lib)
subdirs(swig)
subdirs(python)
subdirs(grc)
subdirs(apps)
subdirs(docs)

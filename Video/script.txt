Hi!

My name is Man Ho.

My name is Andrew Lanez.

My name is Vahid Nazer.

We are in the middle of an information revolution and the world is rapidly becoming connected.

An estimated 50 billion devices will be connected to the internet by 2020.

People stay connected through Facebook, Twitter, and Instagram.

But what about staying connected to other things in our lives that can't update themselves on social media?

Your car?

Your luggage?

Your newborn baby?

Sure, GPS devices exist, but there are so many other untapped technologies that form the internet of things ecosystem.

You may ask, "What is IoT?"
IoT is a network of embedded devices, vehicles, buildings, and other physical objects to which users can stay connected.

Where ever a wireless network exists, you should be able to find your stuff.

There was a time when I flew from Vietnam back to San Diego.
I went to the baggage claim and waited.
After everyone else picked up their baggage, I was the only one left and found out my baggage was delayed.
It took 3 hours for the airline to reroute my baggage to me.

That experience inspired me and my team to design this beacon tracking system.
Our system incorporates the emerging technology of IoT.

Our system design works as a beacon device that's placed inside the luggage.
The beacon then transmits the GPS data wirelessly to a receiver.
The receiver then relays the signal to a mobile phone.
The phone app then transmits the data to the cloud.
The user can login to the cloud to track their luggage on Google Maps.
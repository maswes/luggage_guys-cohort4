Hi!

My name is Man Ho.

My name is Andrew Lanez.

My name is Vahid Nazer.

We are in the middle of an information revolution and the world is rapidly becoming connected.

An estimated 50 billion devices will be connected to the internet by 2020.

People stay connected through Facebook, Twitter, and Instagram.

But what about staying connected to other things in our lives that can't update themselves on social media?

Your car?

Your luggage?

Your newborn baby?

There was a time when I flew from Vietnam back to San Diego.
I went to the baggage claim and waited.
After everyone else picked up their baggage, I was the only one left and found out my baggage was delayed.
It took 3 hours for the airline to reroute my baggage to me.

That experience inspired me and my team to design this beacon tracking system.

Where ever a wireless network exists, you should be able to find your stuff.

Our system incorporates the emerging technology of IoT.

/* OMIT OMIT OMIT
Sure, GPS devices exist, but there are so many other untapped technologies that form the internet of things ecosystem.
OMIT OMIT OMIT */

You may ask, "What is IoT?"
IoT is a network of embedded devices, vehicles, buildings, and other physical objects to which users can stay connected.

OFDM is the robust wireless technology shared amongs 4G LTE, WiFi, and DirectTv.
We have integrated OFDM into the design of our wireless tracking system.
Why is it robust? Because if data gets impaired during transmission to the cloud,
OFDM can easily recover that data.

For prototyping, we used Software Defined Radio (SDR).
Because it is software, it gives us the advantage of rapid development.
We use a hybrid of software with an FPGA which is programmable hardware.
This combination accelerates system performance.
We benchamarked our FPGA components to be 80% faster than their software counterparts.

Our system design works as a beacon device that's placed inside the luggage.
The beacon then transmits the GPS data wirelessly to a receiver.
The receiver then relays the signal to a mobile phone.
The phone app then transmits the data to the cloud.
The user can login to the cloud to track their luggage on Google Maps.

So let's step outside for a demo.

We're going to show you a live demo of our Beacon Tracking System prototype.
Inside here, we have the system.
On the left side is the transmitter which has the software defined radio, an FPGA, and USRP.
On the right side is the receiver which has the same.
So outside, we've got the transmitter hooked up to two antennas.
We've got our GPS antenna receiving data from the satellite and the transmitter that transmits OFDM data.
And on the other side, we've got the receiver's antenna receiving the OFDM data.
So now we're going to show you how it all works with the Internet of Things.

Roll support video clips {

Again, the portion of our prototype system sitting on the left side represents the beacon that gets dropped into your luggage. That signal gets received by your surrounding wireless network modeled by the receiver on the right side. This receiver is connected to the cloud, updating your luggage's location in Google Maps.

The journey getting up to this point has been fun but very challenging.

We faced many challenges getting our hardware to work reliably in real time.
By pooling together our uniqure skills and receiving advice from professors and teaching assistants,
we have overcome all of our challenges to get this system up and running.

} // End support video clips

This project has been a very satisfying experience.
After we graduate from this program, we envision continuing to work together to identify problems, 
build solutions, and hopefully make a business out of it.
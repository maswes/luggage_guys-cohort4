# Cohort 4 Team: Luggage Guys #
Man Ho | Andrew Lanez | Vahid Nazer  
http://www.theluggageguys.com

###What is this project about?###
![Luggage.png](https://bitbucket.org/repo/MBnA85/images/88433486-Luggage.png)

This project focuses on the design and development of an in-baggage beacon tracking device which uses wireless connectivity to locate baggage through the use of a wireless embedded system and internet of things (IoT) cloud platform.


###The solution###
![Solution.png](https://bitbucket.org/repo/MBnA85/images/1767580462-Solution.png)

With users able to track their luggage from their mobile phone and messages confirming safe arrival, this technology could help passengers fly with the knowledge that if their bag happens to be misdirected, they can help the airport locate and send it to the correct location.  
  

# System Overview #

![top.png](https://bitbucket.org/repo/MBnA85/images/3528744317-top.png)
The diagram above is a high level system diagram of our system. The beacon is dropped into the luggage and its GPS coordinates and unique User ID (UID) is received by airport base stations or a receiver-enabled mobile device (this may be yours and anyone who you let opt in to your tracking network). From there, the information is uploaded to a server and can be tracked from a web browser on the internet.  
  
  

![mid.png](https://bitbucket.org/repo/MBnA85/images/3389222844-mid.png)
The diagram above drills into the components of the beacon transmitter and receiver. The transmitter consists of a GPS antenna, Zedboard, and USRP. The receiver is a USRP and Zedboard which interfaces with the mobile device.
  
  

![bottom.png](https://bitbucket.org/repo/MBnA85/images/68683497-bottom.png)
Above are the inner workings of the transmitter/receiver. The GPS antenna interfaces with GNU Radio (running on Xillinux OS bootable from SD card). GNU Radio also interfaces with the programmable logic (PL) via Xillybus where the FFT block is optimized on hardware. The IQ data is sent over ethernet to the USRP (N210) RF frontend and received by the receiver's USRP RF frontend. GNU Radio takes the IQ data over ethernet. Baseband data eventually passes through the Frequency Modulator block (used for frequency synchronization) optimized on the PL via Xillybus, back to GNU Radio for final processing. UID and GPS coordinates are ultimately demodulated and sent to the mobile device which forwards that data to the server for user tracking.
  
  

# Information Repository #

The following are links to technical discussion and steps taken to bring our system up and running, source code, and links to demo videos.

* [Source Repository](https://bitbucket.org/luggageguys/luggage_guys/src)
* Capstone Project Milestones
    * [Class 1 Milestones/Report](https://bitbucket.org/luggageguys/luggage_guys/raw/7aa8fe3b6c8ddbca9c5a49aa7cdadfdc910877d0/Milestone/Capstone_Class1_Report.pdf)
    * [Class 2 Milestones/Report](https://bitbucket.org/luggageguys/luggage_guys/src/553502748c2a576aaffd5be90ba090aa53aff680/Milestone/Capstone_Class2_Report.pptx) [[Video 1](https://www.youtube.com/watch?v=ituv-mTL9Mg)] [[Video 2](https://www.youtube.com/watch?v=whaAxFmf4GI)]
    * [Class 3 Milestones/Report](https://bitbucket.org/luggageguys/luggage_guys/raw/7ef5ded34356c1fc884298845b4ba284aa594f1e/Milestone/Capstone%20Class%203%20Report%20-%20Vahid_Man_Andrew.pptx) [[Video](https://www.youtube.com/watch?v=XJ-Eh8BHVso)]
    * [Class 4 Milestones/Report](https://bitbucket.org/luggageguys/luggage_guys/raw/5ee79ddb4b21f1954240cf49e9c498398e3cd1c3/Milestone/Capstone%20Class%204%20Report%20-%20Vahid_Man_Andrew.pptx) [[Video](https://youtu.be/R6TyKLMpEqM)]
    * [Class 5 Milestones/Report](https://bitbucket.org/luggageguys/luggage_guys/raw/dad6e9bf3137103a5fde58b4b49ed765b8984d20/Milestone/Capstone%20Class%205%20Report%20-%20Vahid_Man_Andrew.pptx) [[Video](https://youtu.be/gwjC6f0Tvys)]
* [Course WES269 Progress Reports](https://bitbucket.org/luggageguys/luggage_guys/wiki/Course%20WES269%20Progress%20Reports)
* [Demo Webpage Link](http://www.theluggageguys.com)
* Technical Resources
    * [Xillinux and GNU Radio Setup on SD card](https://bitbucket.org/luggageguys/luggage_guys/wiki/Xillinux%20and%20GNU%20Radio%20Setup%20on%20SD%20Card)
    * [Setup Control Port Monitor in GNU Radio](https://bitbucket.org/luggageguys/luggage_guys/wiki/Setup%20Control%20Port%20Monitor%20in%20GNU%20Radio)
    * [How to Create Custom OOT (Out-of-Tree) Modules in GNU Radio](https://bitbucket.org/luggageguys/luggage_guys/wiki/How%20to%20Create%20Custom%20OOT%20(Out-of-Tree)%20Modules%20in%20GNU%20Radio)
    * [Zedboard vs VMWare - Bottlenecks](https://bitbucket.org/luggageguys/luggage_guys/wiki/Zedboard%20vs%20VMWare%20-%20Bottlenecks)